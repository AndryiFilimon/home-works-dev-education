package com.github.stringone.StringASCII;

public class StringASCII {
    public static void main(String[] args) {
        for (char i = 32; i < 127; i++) {
            System.out.println((char)i);
        }
    }
}