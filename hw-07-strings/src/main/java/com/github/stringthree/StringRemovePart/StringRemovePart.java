package com.github.stringthree.StringRemovePart;

public class StringRemovePart {

    public static String stringRemovePart(){
        String str = "Morning is coming and I'm writing code";
        int startNum = 21;
        int endNum = str.length();
        str = str.substring(startNum, endNum);
        return str;
    }
}
