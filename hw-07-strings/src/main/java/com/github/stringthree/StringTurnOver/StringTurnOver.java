package com.github.stringthree.StringTurnOver;

public class StringTurnOver {

    public static String stringTurnOver(String str){
        StringBuilder stringBuilder = new StringBuilder(str);
        return stringBuilder.reverse().toString();
    }
}
