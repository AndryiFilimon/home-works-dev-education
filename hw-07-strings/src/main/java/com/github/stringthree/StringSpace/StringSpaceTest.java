package com.github.stringthree.StringSpace;

import org.junit.Assert;
import org.junit.Test;

public class StringSpaceTest {

    @Test

    public void stringSpaceTest(){
        String s = "no space,and life,is bad";
        String exp = "no space, and life, is bad";
        String act = s.replaceAll("(?<=\\p{Punct})(?=\\w)", " ");
        Assert.assertEquals(exp, act);
    }
}
