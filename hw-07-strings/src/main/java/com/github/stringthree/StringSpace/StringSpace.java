package com.github.stringthree.StringSpace;

public class StringSpace {

    public static String stringSpace() {
        String s = "no space,and life,is bad";
        String st = s.replaceAll("(?<=\\p{Punct})(?=\\w)", " ");
        return st;
    }
}