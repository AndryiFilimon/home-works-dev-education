package com.github.stringthree.StringShort;

public class StringShort {
    public static int stringShort(String str) {

        String st = str.replaceAll("\\p{Punct}", "");
        String[] arr = st.split(" ");

        int lenWord = arr[0].length();
        for (int i = 1; i < arr.length; i++) {
            if (arr[i].length() < lenWord) {
                lenWord = arr[i].length();
            }
        }
        return lenWord;
    }
}
