package com.github.stringthree.StringShort;

import org.junit.Assert;
import org.junit.Test;

public class StringShortTest {


    @Test
    public void stringShort() {
        int exp = 2;
        int act = StringShort.stringShort("Why is it so hard, Japanese policeman");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void stringShortLittle() {
        int exp = 2;
        int act = StringShort.stringShort("is");
        Assert.assertEquals(exp, act);
    }

    @Test
    public void stringShortEmpty() {
        int exp = 0;
        int act = StringShort.stringShort("");
        Assert.assertEquals(exp, act);
    }
}
