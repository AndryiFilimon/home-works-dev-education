package com.github.stringthree.StringCopy;

public class StringCopy {

    public static String stringCopy(String str) {
        int strCop = str.length();
        String result = "";
        for (int i = 0; i < strCop; i++) {
            if (!result.contains(str.charAt(i) + ""))
                result += str.charAt(i);
        }
        return result;
    }
}
