package com.github.stringthree.StringLastThree;

import org.junit.Assert;
import org.junit.Test;

public class StringLastThreeTest {

    @Test
    public void stringLastThreeArray() {
        String[] exp = {"dd$$$", "ggg$$$", "hh$$$"};
        String[] act = StringLastThree.stringLastThree(new String[]{"ddddd", "gggggg", "hhhhh"});
        Assert.assertArrayEquals(exp, act);
    }

    @Test
    public void arrayNull() {
        String[] act = StringLastThree.stringLastThree(null);
        Assert.assertNull(act);
    }
}
