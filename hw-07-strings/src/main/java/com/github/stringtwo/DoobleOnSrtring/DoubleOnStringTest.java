package com.github.stringtwo.DoobleOnSrtring;

import org.junit.Assert;
import org.junit.Test;

public class DoubleOnStringTest {
    @Test
    public void doubleOnSrtringTest() {
        double v = 32.32;
        String exp = "32.32";
        String act = Double.toString(v);
        Assert.assertEquals(exp, act);
    }
}
