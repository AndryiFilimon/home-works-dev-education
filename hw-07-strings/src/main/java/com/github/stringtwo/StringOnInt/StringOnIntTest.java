package com.github.stringtwo.StringOnInt;

import org.junit.Assert;
import org.junit.Test;

public class StringOnIntTest {

    @Test
    public void stringOnIntTest(){
        String ss = "121";
        int exp = 121;
        int act = Integer.parseInt(ss);
        Assert.assertEquals(exp, act);
    }
}
