package com.github.stringtwo.IntOnString;

import org.junit.Assert;
import org.junit.Test;

public class IntOnStringTest {
    @Test
    public void stringOnIntTest(){
        int i = 123;
        String exp = "123";
        String act = Integer.toString(i);
        Assert.assertEquals(exp, act);
    }
}
