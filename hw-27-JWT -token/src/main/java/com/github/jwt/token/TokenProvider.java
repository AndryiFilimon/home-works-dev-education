package com.github.jwt.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class TokenProvider {

    public final int VALID_SECOND = 24 * 60 * 60;
    private static final Logger LOGGER = Logger.getLogger(TokenProvider.class.getName());
    private static final String AUTH_KEY = "auth";
    private String secretKey;
    private long tokenValidity;
    private long tokenValidityRem;

    public void init() {
        this.secretKey = "secretKey";
        this.tokenValidity = TimeUnit.SECONDS.toMillis(10);
        this.tokenValidityRem = TimeUnit.SECONDS.toMillis(VALID_SECOND);
    }

    public String generateToken(String userName, Set<String> auth, Boolean rem) {
        long now = (new Date()).getTime();
        long validity = rem ? tokenValidityRem : tokenValidity;

        return Jwts.builder()
                .setSubject(userName)
                .claim(AUTH_KEY, String.join(",", auth))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setExpiration(new Date(now + validity))
                .compact();
    }

    public JWTCredential getCredential(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody();

        Set<String> authorization = Arrays.stream((claims.get(AUTH_KEY).toString().split(",")))
                .collect(Collectors.toSet());
        return new JWTCredential(claims.getSubject(), authorization);
    }

    public boolean isValidToken(String authToken) {
        try{
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            LOGGER.log(Level.INFO, "Invalid JWT signature: {0}", e.getMessage());
            return false;
        }
    }
}
