package com.github.jwt.token;

import java.util.Set;

public class JWTCredential {

    private final String principal;
    private final Set<String> authorization;

    public JWTCredential(String principal, Set<String> authorization) {
        this.principal = principal;
        this.authorization = authorization;
    }

    public String getPrincipal() {
        return principal;
    }

    public Set<String> getAuthorization() {
        return authorization;
    }
}
