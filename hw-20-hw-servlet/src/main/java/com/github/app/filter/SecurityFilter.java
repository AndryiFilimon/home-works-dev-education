package com.github.app.filter;

import com.github.app.bean.UserAccount;
import com.github.app.request.UserRoleRequestWrapper;
import com.github.app.utils.AppUtils;
import com.github.app.utils.SecurityUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        String servletPath = request.getServletPath();

        UserAccount loginedUser = AppUtils.getLoginedUser(request.getSession());

        if (servletPath.equals("/login")) {
            chain.doFilter(request, response);
            return;
        }
        HttpServletRequest wrapRequest = request;

        if (loginedUser != null) {
            String userName = loginedUser.getUserName();

            List<String> roles = loginedUser.getRoles();

            wrapRequest = new UserRoleRequestWrapper(userName, roles, request);
        }
        if (SecurityUtils.isSecurityPage(request)) {
            if (loginedUser == null) {
                String requestUri = request.getRequestURI();
                int redirect = AppUtils.storeRedirectAfterLoginUrl(request.getSession(), requestUri);

                response.sendRedirect(wrapRequest.getContextPath() + "/login?redirect=" + redirect);
                return;
            }
            boolean hasPermission = SecurityUtils.hasPermission(wrapRequest);
            if (hasPermission) {
                RequestDispatcher dispatcher
                        = request.getServletContext().getRequestDispatcher("/WEB-INF/views/accessDeniedView.jsp");
                dispatcher.forward(request, response);
                return;
            }
            chain.doFilter(wrapRequest, response);
        }
    }

    @Override
    public void destroy() {

    }
}
