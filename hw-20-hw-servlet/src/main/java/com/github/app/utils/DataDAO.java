package com.github.app.utils;

import com.github.app.bean.UserAccount;
import com.github.app.config.SecurityConfig;

import java.util.HashMap;
import java.util.Map;

public class DataDAO {

    private static final Map<String, UserAccount> mapUsers = new HashMap<String, UserAccount>();

    static {
        initUsers();
    }

    private static void initUsers() {
        UserAccount emp = new UserAccount("user", "1234", UserAccount.GENDER_MALE, //
                SecurityConfig.ROLE_EMPLOYEE);

        UserAccount mng = new UserAccount("manager", "4321", UserAccount.GENDER_FEMALE, //
                SecurityConfig.ROLE_MANAGER, SecurityConfig.ROLE_EMPLOYEE);

        mapUsers.put(emp.getUserName(), emp);
        mapUsers.put(mng.getUserName(), mng);
    }

    public static UserAccount findUser(String userName, String password) {
        UserAccount u = mapUsers.get(userName);
        if (u != null && u.getPassword().equals(password)) {
            return u;
        }
        return null;
    }
}
