package com.github.app.servlet;

import com.github.app.bean.UserAccount;
import com.github.app.utils.AppUtils;
import com.github.app.utils.DataDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    RequestDispatcher dispatcher
            = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");
    dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        UserAccount userAccount = DataDAO.findUser(userName, password);

        if(userAccount == null){
            String errorMessage = "invalid userName or Password";
            request.setAttribute("errorMessage", errorMessage);
            RequestDispatcher dispatcher
                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");
            dispatcher.forward(request, response);
            return;
        }
        AppUtils.storeLoginedUser(request.getSession(), userAccount);
        int redirect = -1;
        redirect = Integer.parseInt(request.getParameter("redirect"));
        String requestUri = AppUtils.getRedirectAfterLoginUrl(request.getSession(), redirect);
        if(requestUri != null){
            response.sendRedirect(requestUri);
        } else {
            response.sendRedirect(request.getContextPath() + "/userInfo");
        }
    }
}
