package com.github.aplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.security.NoSuchAlgorithmException;

public class ConnectionHandler implements Runnable{

    private final Logger log = LoggerFactory.getLogger(ConnectionHandler.class);

    InputStream in;
    OutputStream out;
    String line;

    private Socket socket;

    public ConnectionHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try{
            in = socket.getInputStream();
            out = socket.getOutputStream();
        } catch (IOException e) {
            log.error("Enter {}: " + e);
        }
        try{
            WebHandler.handShake(in, out);
            out.write(WebHandler.encode("Are you happy?"));
            out.flush();
            WebHandler.printInputStream(in);
        } catch (IOException | NoSuchAlgorithmException e) {
            log.error("Enter {}: " + e);
        }
        while (true) {
            try{
                this.line = WebHandler.printInputStream(in);
                if(line == null) break;
                System.out.println("You say " + line);
                out.write(WebHandler.encode("You say: " + line));
            } catch (SocketTimeoutException e) {
                log.warn("Connection timeout");
                log.error("Enter {}: " + e);
            } catch (IOException e) {
                log.error("Enter {}: " + e);
            }
        }
    }
}