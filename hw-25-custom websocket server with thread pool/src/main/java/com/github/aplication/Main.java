package com.github.aplication;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) throws IOException {
        ExecutorService service = Executors.newFixedThreadPool(10);
        ServerSocket serverSocket = new ServerSocket(8082);
        while(true) {
            Socket socket = serverSocket.accept();
            socket.setSoTimeout(10000);
            service.execute(new ConnectionHandler(socket));
        }
    }
}
