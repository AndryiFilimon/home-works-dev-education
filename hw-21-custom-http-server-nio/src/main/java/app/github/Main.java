package app.github;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class Main {

    public static void main(String[] args) throws IOException {

        ServerSocketChannel socketChannel = ServerSocketChannel.open();
        socketChannel.socket().bind(new InetSocketAddress(8077));

        while (true){
            SocketChannel client = socketChannel.accept();
            System.out.println("ConnectionSet: " + client.getRemoteAddress());
            HttpHandler handler = new HttpHandler(client);
            handler.formatResponse();
            System.out.println("Socket close");
            client.close();
        }
    }
}
