package com.github.paint_swing.config;

import com.github.paint_swing.formats.BaseFormat;
import com.github.paint_swing.formats.impl.format.*;
import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class ImageFormatChoos {

    public static BaseFormat chooseFormat(File file) {
        switch (FilenameUtils.getExtension(file.getName())) {
            case "png":
                return new PngFormat(file);
            case "jpeg":
                return new JpegFormat(file);
            case "bmp":
                return new BmpFormat(file);
            case "bin":
                return new BinFormat(file);
            case "csv":
                return new CsvFormat(file);
            case "json":
                return new JsonFormat(file);
            case "xml":
                return new XmlFormat(file);
            case "yml":
                return new YmlFormat(file);
        }
        throw new IllegalArgumentException();
    }
}
