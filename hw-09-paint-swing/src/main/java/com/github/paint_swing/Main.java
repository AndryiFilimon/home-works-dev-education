package com.github.paint_swing;

import com.github.paint_swing.view.gui.Paint;
import com.github.paint_swing.view.gui.impl.RightDividedPanelPaint;

public class Main {

    public static void main(String[] args) {

        Paint paint = new RightDividedPanelPaint();
        paint.show();
    }
}