package com.github.paint_swing.view.gui;

import com.github.paint_swing.view.DrawPanel;

import javax.swing.*;

public abstract class Paint {

    protected JFrame paintFrame;

    protected DrawPanel drawPanel;

    public void clearButtonSetUp() {

    }

    public void colorButtonSetUp() {

    }

    public void openButtonSetUp() {

    }

    public void saveButtonSetUp() {

    }

    public void thicknessSetUp() {

    }

    public void ellipseButtonSetUp() {

    }

    public void lineButtonSetUp() {

    }

    public void pencilButtonSetUp() {

    }

    public void rectangleButtonSetUp() {

    }

    public void roundRectangleButtonSetUp() {

    }


    public abstract void show();

    public JButton clearButton = new JButton("Clear");

    public JButton colorButton = new JButton("Color");

    public JTable colorField = new JTable(1, 1);

    public JButton saveButton = new JButton("Save");

    public JButton openButton = new JButton("Open");

    public JLabel thicknessLabel = new JLabel("Thickness");

    public JTextField thicknessField = new JTextField();

    public JSlider thicknessSlider = new JSlider();

    public JButton ellipseButton = new JButton("Ellipse");
    public JButton lineButton = new JButton("Line");
    public JButton pencilButton = new JButton("Pencil");
    public JButton rectangleButton = new JButton("Rectangle");
    public JButton roundRectangleButton = new JButton("Round rectangle");
}
