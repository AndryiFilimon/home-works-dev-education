package com.github.paint_swing.view.gui.impl;

import com.github.paint_swing.config.ImageFormatChoos;
import com.github.paint_swing.formats.BaseFormat;
import com.github.paint_swing.view.DrawPanel;
import com.github.paint_swing.view.gui.Paint;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;



public class ControlPanelSimplePaint extends Paint {

    BaseFormat format;

    Paint paint;

    @Override
    public void colorButtonSetUp() {
        paint.colorButton.addActionListener(e -> {
            Color color = JColorChooser.showDialog(new JFrame(), "Color", Color.RED);
            drawPanel.setColor(color);
            colorButton.setBackground(color);
        });
    }

    @Override
    public void clearButtonSetUp() {
        paint.clearButton.addActionListener(e -> drawPanel.clear());
    }

    @Override
    public void saveButtonSetUp() {
        paint.saveButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filterImages = new FileNameExtensionFilter(
                    ".png, .jpeg, .bmp",
                    "png", "jpeg", "bmp"
            );
            FileNameExtensionFilter filterObjects = new FileNameExtensionFilter(
                    ".bin, .csv, .json, .xml, .yml",
                    "bin", "csv", "json", "xml", "yml"
            );
            fileChooser.addChoosableFileFilter(filterImages);
            fileChooser.addChoosableFileFilter(filterObjects);
            int rVal = fileChooser.showSaveDialog(paintFrame);
            if(rVal == JFileChooser.APPROVE_OPTION){
                File file = fileChooser.getSelectedFile();
                format = ImageFormatChoos.chooseFormat(file);
                format.save(this.drawPanel.getImage());
            }
        });
    }

    @Override
    public void openButtonSetUp() {
        paint.openButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            FileNameExtensionFilter filterImages = new FileNameExtensionFilter(
                    ".png, .jpeg, .bmp",
                    "png", "jpeg", "bmp"
            );
            FileNameExtensionFilter filterObjects = new FileNameExtensionFilter(
                    ".bin, .csv, .json, .xml, .yml",
                    "bin", "csv", "json", "xml", "yml"
            );
            fileChooser.addChoosableFileFilter(filterImages);
            fileChooser.addChoosableFileFilter(filterObjects);
            int rVal = fileChooser.showOpenDialog(paintFrame);
            if(rVal == JFileChooser.APPROVE_OPTION){
                File file = fileChooser.getSelectedFile();
                format = ImageFormatChoos.chooseFormat(file);
                this.drawPanel.setImage(format.load());
            }
        });
    }

    @Override
    public void show() {
        paintFrame = new JFrame("Swing Paint");
        Container content = paintFrame.getContentPane();
        content.setLayout(new BorderLayout());
        drawPanel = new DrawPanel();

        content.add(drawPanel, BorderLayout.CENTER);

        JPanel controlPanel = new JPanel();

        clearButtonSetUp();
        controlPanel.add(clearButton);
        colorButtonSetUp();
        controlPanel.add(colorButton);
        thicknessSetUp();
        controlPanel.add(paint.thicknessSlider);
        openButtonSetUp();
        controlPanel.add(openButton);
        saveButtonSetUp();
        controlPanel.add(saveButton);

        content.add(controlPanel, BorderLayout.NORTH);

        paintFrame.setSize(734, 553);
        paintFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        paintFrame.setVisible(true);
    }
}