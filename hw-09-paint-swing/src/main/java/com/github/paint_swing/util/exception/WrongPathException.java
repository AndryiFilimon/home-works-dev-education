package com.github.paint_swing.util.exception;


public class WrongPathException extends RuntimeException{

    public WrongPathException(String errorMessage){
        super(errorMessage);
    }

}
