package com.github.paint_swing.util;


public enum Figure {
    ELLIPSE,
    LINE,
    PENCIL,
    RECTANGLE,
    ROUND_RECTANGLE
}
