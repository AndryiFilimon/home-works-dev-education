package com.github.paint_swing.util.exception;


public class WrongFormatException extends RuntimeException{

    public WrongFormatException(String errorMessage){
        super(errorMessage);
    }
}
