package com.github.paint_swing.formats;

import java.awt.*;

public interface BaseFormat {

    Image load();

    boolean save(Image image);

}
