package com.github.paint_swing.formats.impl.format;

import com.github.paint_swing.formats.BaseFormat;
import com.github.paint_swing.mod.CustomLine;
import com.github.paint_swing.util.FileUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class BinFormat implements BaseFormat {

    private final File file;

    public BinFormat(File file) {
        this.file = file;
    }

    @Override
    public Image load() {
        byte[] bytes = FileUtils.readBinFile(file);
        CustomLine customLine = new CustomLine();
        try(ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)){
            customLine = (CustomLine) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return customLine.toBufferedImage();
    }

    @Override
    public boolean save(Image image) {
        if(!this.file.exists()){
            FileUtils.createFile(this.file);
        }
        byte[] bytes = null;
        CustomLine customLine = new CustomLine((BufferedImage) image);
        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)){
            objectOutputStream.writeObject(customLine);
            bytes = byteArrayOutputStream.toByteArray();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return FileUtils.writeToBinFile(file, bytes);
    }

}