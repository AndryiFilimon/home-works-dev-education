package com.github.paint_swing.formats.impl.format;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.github.paint_swing.formats.BaseFormat;
import com.github.paint_swing.mod.CustomLine;
import com.github.paint_swing.util.exception.WrongFormatException;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CsvFormat implements BaseFormat {

    private final File file;

    public CsvFormat(File file) {
        this.file = file;
    }

    @Override
    public Image load() {
        CustomLine customLine;
        CsvMapper mapper = new CsvMapper();
        try {
            customLine = mapper.readValue(this.file, CustomLine.class);
        } catch (IOException e) {
            throw new WrongFormatException("Failed to read a file.");
        }
        return customLine.toBufferedImage();
    }

    @Override
    public boolean save(Image image) {
        CustomLine customLine = new CustomLine((BufferedImage) image);
        CsvMapper mapper = new CsvMapper();
        try {
            mapper.writeValue(this.file, customLine);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}