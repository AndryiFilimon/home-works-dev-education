package com.github.paint_swing.formats.impl.format;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.github.paint_swing.formats.BaseFormat;
import com.github.paint_swing.mod.CustomLine;
import com.github.paint_swing.util.exception.WrongFormatException;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class XmlFormat implements BaseFormat {

    private final File file;

    public XmlFormat(File file) {
        this.file = file;
    }

    @Override
    public Image load() {
        CustomLine customLine;
        XmlMapper mapper = new XmlMapper();
        try {
            customLine = mapper.readValue(this.file, CustomLine.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new WrongFormatException("Failed to read a file.");
        }
        return customLine.toBufferedImage();
    }

    @Override
    public boolean save(Image image) {
        CustomLine customLine = new CustomLine((BufferedImage) image);
        XmlMapper mapper = new XmlMapper();
        try {
            mapper.writeValue(this.file, customLine);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

}