let data = {
    Jeannie_23: {
        name: "Jeannie Randolph",
        pass: "$3z&Se8Ph8",
    },

    k_brown: {
        name: "Kathie Brown",
        pass: "*7Gf5iNy7@",
    },

    mega_powers: {
        name: "Powers Justice",
        pass: "8*DkE87jb&",
    },

    o_neal: {
        name: "Morales Oneal",
        pass: "68Zp6-@vUp",
    },

    A_dela: {
        name: "Adela Heath",
        pass: "83-2pgJSj$",
    }
}

const root = document.getElementById('root');

const form = document.createElement('form');
form.setAttribute('class', 'form');

const form__div = document.createElement('div');
form__div.setAttribute('class', 'form__div');

const firstName = document.createElement('input');
firstName.setAttribute('class', 'firstName input');
firstName.setAttribute('placeholder', ' firstName');

const lastName = document.createElement('input');
lastName.setAttribute('class', 'lastName input');
lastName.setAttribute('placeholder', ' lastName');

const password = document.createElement('input');
password.setAttribute('class', 'password input');
password.setAttribute('type', 'password');
password.setAttribute('placeholder', ' password')
password.setAttribute('reqired', 'true');

const btn = document.createElement('button');
btn.setAttribute('class', 'btn');
btn.setAttribute('type', 'button');

const txtnode = document.createTextNode('Send');

btn.appendChild(txtnode);

form__div.appendChild(firstName);
form__div.appendChild(lastName);
form__div.appendChild(password);
form__div.appendChild(btn);

form.appendChild(form__div)
root.appendChild(form);

firstName.addEventListener('input', (e) => {
    const reg = /^[A-Za-z-]/;
    let checkFirstName = reg.test(firstName.value);
    if (e.target.value.length < 2) {
        firstName.style.borderColor = 'red';
    } else if (e.target.value.length > 10) {
        firstName.value = e.target.value.substr(0, 10);
    } else {
        firstName.style.borderColor = 'black';
    }
    if (checkFirstName !== true) {
        firstName.style.borderColor = 'red';
    }
})

lastName.addEventListener('input', (e) => {
    const reg = /^[A-Za-z-]/;
    let checkLastName = reg.test(lastName.value);
    if (e.target.value.length < 2) {
        lastName.style.borderColor = 'red';
    } else if (e.target.value.length > 20) {
        lastName.value = e.target.value.substr(0, 20)
    } else {
        lastName.style.borderColor = 'black';
    }
    if (checkLastName !== true) {
        lastName.style.borderColor = 'red';
    }
})

password.addEventListener('input', (e) => {
    const reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$&@*-])[A-Za-z\d$&@*-]/;
    let checkPassword = reg.test(password.value);
    if (e.target.value.length < 5) {
        password.style.borderColor = 'red';
    } else if (e.target.value.length > 10) {
        password.value = e.target.value.substr(0, 10)
    } else {
        password.style.borderColor = 'black'
    }
    if (checkPassword !== true) {
        password.style.borderColor = 'red';
    }
})

btn.addEventListener('click', () => {
    sendIn()
});

function sendIn(){
    let firstNameVal = firstName.value;
    let lastNameVal = lastName.value;
    let passwordVal = password.value;

    let fieldsCounter = 0;
    const singleField = 'Field';
    const manyField = 'Fields';
    const reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$&@*-])[A-Za-z\d$&@*-]{5,10}$/;
    let fields = '';
    const fullName = firstNameVal + "" + lastNameVal;
    let checkReg = reg.test(password.value);
    if(checkReg !== true) {
        password.style.borderColor = '1px solid red';
    } else {
        password.style.borderColor = 'black';
    }
    if(firstName.value === '' || lastName.value === '' || checkReg !== true){
        let fieldsToShow = '';
        let alertString = '';
        if(firstNameVal === ''){
            fieldsToShow += 'firstname'
            fieldsCounter++;
        }
        if(lastNameVal === ''){
            fieldsToShow += 'lastname'
            fieldsCounter++;
        }
        if(passwordVal === ''){
            fieldsToShow += 'password'
            fieldsCounter++;
        } else if (checkReg === false){
            fieldsToShow += 'the password should only consist of letters, numbers and special characters $, &, @, *';
        }
        if(fieldsCounter < 2) {
            alertString = singleField + '' + fieldsToShow + 'required to fill';
        } else {
            alertString = manyField + '' + fieldsToShow + 'required to fill';
        }
        alert('Validation error:' + alertString);
    } else {
        auth(fullName, passwordVal);
    }
}

function auth(name, password) {
    let resp = false;
    let i = 0;
    let userName = '';
    while (i <data.length) {
        if(data[i].name === name && data[i].pass === password){
            userName = data[i].username;
            resp = true;
            i = data.length + 1;
        }
        i++;
    }
    if(resp === true) {
        alert('You have successfully logged in as:' + userName + "," + name)
    } else {
        alert('Authentication error')
    }
}

window.addEventListener('keypress', (e) => {
    if(e.key === 'Enter') {
        sendIn()
    }
})








