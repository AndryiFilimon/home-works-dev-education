package com.github.httpserver.variant1;

import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.util.Enumeration;

class RequestInfo {
    static String br = "<br>";

    public static void printToBrowser(PrintWriter out, HttpServletRequest req) {
        out.println("Method: " + req.getMethod());
        out.print(br + "Request URI: " + req.getRequestURI());
        out.print(br + "Protocol: " + req.getProtocol());
        out.print(br + "PathInfo: " + req.getPathInfo());
        out.print(br + "Remote Address: " + req.getRemoteAddr());
        Enumeration<String> enn = req.getHeaderNames();
        out.print(br + "Header INFO: ");
        while (enn.hasMoreElements()) {
            String name = (String) enn.nextElement();
            String value = req.getHeader(name);
            out.print(br + name + " = " + value);
        }
    }
}