package com.github.httpserver.variant1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

@WebServlet("/RequestServlet")
public class RequestServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private int count = 0;


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        performTask(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        performTask(request, response);
    }

    private void performTask(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException {
        try {
            resp.setContentType("text/html; charset=UTF-8");
            PrintWriter out = resp.getWriter();
            count = ClickOutput.printClick(out, count);
            if (count == 1)
                RequestInfo.printToBrowser(out, req);
            out.close();
        } catch (UnsupportedEncodingException e) {
            System.err.print("UnsupportedEncoding");
        } catch (IOException e) {
            System.err.print("IOException");
        }
    }
}