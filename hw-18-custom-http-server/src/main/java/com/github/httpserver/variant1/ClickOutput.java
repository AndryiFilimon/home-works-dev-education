package com.github.httpserver.variant1;

import java.io.PrintWriter;

class ClickOutput {
    public static int printClick(PrintWriter out, int count) {
        out.print(++count + " -appeal." + "<br>");
        return count;
    }
}