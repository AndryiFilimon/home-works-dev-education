package com.github.httpserver.var3;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleServerSocket {
    public static void main(String[] args) {
        engSocket();
    }

    public static void engSocket() {

        int port = 8081;
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server started with port " + port);

            while (true) {

                Socket socket = serverSocket.accept();
                InputStream in = socket.getInputStream();
                OutputStream out = socket.getOutputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));

                String str;
                int k = -1;
                while ((str = br.readLine()) != null && !str.isEmpty()) {
                    int i = str.indexOf("Content-Length");
                    if (i != -1) {
                        k = Integer.parseInt(str.substring(i + 16));
                    }
                    System.out.println(str);
                }
                if (k != -1) {
                    char[] chars = new char[k];
                    System.out.println(k);
                    br.read(chars, 0, k);
                    String res = new String(chars);
                    System.out.println("body " + res);
                }

                String outString = "Hello my friends";
                bw.write("HTTP/1.1 200 OK\n\r" +
                        "Content-Type: text/plain\n\r" +
                        "\n\rContent-Length: \n\r" + outString.length() + "\n\r"
                );
                bw.write("\n\r" + outString + "\n\r\n\r");
                System.out.println("Socket closed.");
                bw.flush();
                bw.close();
                br.close();
                socket.close();

            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}