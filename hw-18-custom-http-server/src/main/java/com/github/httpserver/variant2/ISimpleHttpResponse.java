package com.github.httpserver.variant2;

import java.io.IOException;
import java.net.Socket;
import java.util.Map;

public interface ISimpleHttpResponse {

    void setProtocol(String protocol);

    void setStatusCode(int statusCode);

    void setStatusText(String statusText);

    void setHeaders (Map<String, String> headers);

    void setBody(String body);

    void addHeaders(String header, String value);

    void sendResponse(Socket socket) throws IOException;

}
