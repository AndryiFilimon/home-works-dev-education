package com.github.httpserver.variant2;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpHandler implements IHttpHandler {

    private boolean runTrue = true;
    private ServerSocket serverSocket;
    private int port;

    public HttpHandler() {
    }

    public HttpHandler(int port) {
        this.port = port;
    }

    private void choiceMethod(ISimpleHttpRequest req, ISimpleHttpResponse resp) {
        switch (req.getMethod()) {
            case "GET":
                this.doGet(req, resp);
                break;
            case "POST":
                this.doPost(req, resp);
                break;
            case "PUT":
                this.doPut(req, resp);
                break;
            case "DELETE":
                this.doDelete(req, resp);
                break;
            case "HEAD":
                this.doHead(req, resp);
                break;
            case "CONNECT":
                this.doConnect(req, resp);
                break;
            case "OPTIONS":
                this.doOptions(req, resp);
                break;
            case "TRACE":
                this.doTrace(req, resp);
                break;
            case "PATCH":
                this.doPatch(req, resp);
                break;
            default:
                throw new IllegalMethodException("Method " + req.getMethod() + " is allowed.");
        }
    }

    public void stopHandler() {
        this.runTrue = false;
    }

    @Override
    public void doGet(ISimpleHttpRequest req, ISimpleHttpResponse resp) {
        String outString = "<h1>Get method was called.</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("Ok");
        resp.addHeaders("Content-Type", "text/html");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.setBody(outString);
    }

    @Override
    public void doPost(ISimpleHttpRequest req, ISimpleHttpResponse resp) {
        String outString = "<h1>Post method was called.</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("Ok");
        resp.addHeaders("Content-Type", "text/html");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.setBody(outString);
    }

    @Override
    public void doHead(ISimpleHttpRequest req, ISimpleHttpResponse resp) {
        String outString = "<h1>Head method was called.</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("Ok");
        resp.addHeaders("Content-Type", "text/html");
        resp.addHeaders("Content-Length", "" + outString.length());
    }

    @Override
    public void doPut(ISimpleHttpRequest req, ISimpleHttpResponse resp) {
        String outString = "<h1>Put method was called.</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("Ok");
        resp.addHeaders("Content-Type", "text/html");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.setBody(outString);
    }

    @Override
    public void doDelete(ISimpleHttpRequest req, ISimpleHttpResponse resp) {
        String outString = "<h1>Delete method was called.</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("Ok");
        resp.addHeaders("Content-Type", "text/html");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.setBody(outString);
    }

    @Override
    public void doConnect(ISimpleHttpRequest req, ISimpleHttpResponse resp) {
        String outString = "<h1>Connect method was called.</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("Ok");
        resp.addHeaders("Content-Type", "text/html");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.setBody(outString);
    }

    @Override
    public void doOptions(ISimpleHttpRequest req, ISimpleHttpResponse resp) {
        String outString = "<h1>Options method was called.</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("Ok");
        resp.addHeaders("Content-Type", "text/html");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.setBody(outString);
    }

    @Override
    public void doTrace(ISimpleHttpRequest req, ISimpleHttpResponse resp) {
        String outString = "<h1>Trace method was called.</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("Ok");
        resp.addHeaders("Content-Type", "text/html");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.setBody(outString);
    }

    @Override
    public void doPatch(ISimpleHttpRequest req, ISimpleHttpResponse resp) {
        String outString = "<h1>Patch method was called.</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("Ok");
        resp.addHeaders("Content-Type", "text/html");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.setBody(outString);
    }

    @Override
    public void run() {
        try {
            this.serverSocket = new ServerSocket(this.port);
            this.serverSocket.setSoTimeout(15000000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (this.runTrue) {
            try {
                Socket socket = serverSocket.accept();
                ISimpleHttpRequest request = new SimpleHttpRequest();
                ISimpleHttpResponse response = new SimpleHttpResponse();
                request.takeRequest(socket);
                this.choiceMethod(request, response);
                response.sendResponse(socket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
