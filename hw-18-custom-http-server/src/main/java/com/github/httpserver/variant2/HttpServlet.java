package com.github.httpserver.variant2;


import java.util.concurrent.TimeUnit;

public class HttpServlet {

    public static void main(String[] args) {

    IHttpHandler socketHandler = new HttpHandler(8080);

    Thread socketThread = new Thread(socketHandler, "Socket-Handler");
    socketThread.start();
    System.out.println(socketThread.getName() + " is run.");
        try {
            TimeUnit.SECONDS.sleep(30);
            socketHandler.stopHandler();
        } catch (InterruptedException e) {
            socketHandler.stopHandler();
        }
    }
}
