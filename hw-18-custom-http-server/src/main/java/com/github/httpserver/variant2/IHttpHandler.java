package com.github.httpserver.variant2;


public interface IHttpHandler extends Runnable {

    void stopHandler();

    void doGet(ISimpleHttpRequest req, ISimpleHttpResponse resp);

    void doHead(ISimpleHttpRequest req, ISimpleHttpResponse resp);

    void doPost(ISimpleHttpRequest req, ISimpleHttpResponse resp);

    void doPut(ISimpleHttpRequest req, ISimpleHttpResponse resp);

    void doDelete(ISimpleHttpRequest req, ISimpleHttpResponse resp);

    void doConnect(ISimpleHttpRequest req, ISimpleHttpResponse resp);

    void doOptions(ISimpleHttpRequest req, ISimpleHttpResponse resp);

    void doTrace(ISimpleHttpRequest req, ISimpleHttpResponse resp);

    void doPatch(ISimpleHttpRequest req, ISimpleHttpResponse resp);

}