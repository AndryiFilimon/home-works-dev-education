package com.github.httpserver.variant2;

public class IllegalMethodException extends RuntimeException {

    public IllegalMethodException(){

    }
    public IllegalMethodException(String message){
        super(message);
    }
}
