package com.github;

import java.util.Locale;
import java.util.Scanner;
import java.util.Random;

public class Randomaizer {
    public static void main(String[] args) {
        System.out.println("Input minimum (integer, not less then 1)");
        Scanner mn = new Scanner(System.in);
        int n = mn.nextInt();
        int min = 1;
        int max = 500;
        if (n >= 1) min = n;


        System.out.println("Input maximum (integer, not more then 500)");
        Scanner mx = new Scanner(System.in);
        int xx = mx.nextInt();
        if (xx <= 500) max = xx;
        if (max < min) {
            System.out.println("Your maximum is less then minimum");
        }

        int rndm[];
        rndm = new int[max - min + 1];

        for (int i = 0; i < max - min + 1; i++) {
            int diff = max - min;
            Random random = new Random();
            int k = random.nextInt(diff + 1);
            k += min;
            boolean exist = false;
            for (int j = 0; j < i; j++) {
                if (rndm[j] == k) exist = true;
            }
            if (exist == false) {
                rndm[i] = k;
            } else {
                i--;
            }
        }
        for (int z = 0; z < max - min + 1; z++) {
            System.out.println("Input your command: Generate (G), Exit (E), Help (H)");

            Scanner cmd = new Scanner(System.in);

            String x = cmd.nextLine();
            if (x.equals("E")) {
                System.out.println("Are you sure you want to quit? (Y/N)");
                Scanner yx = new Scanner(System.in);
                String xy = yx.nextLine();
                if (xy.equals("Y")) {
                    System.out.println("Good Bye");
                    System.exit(0);
                } else {
                    z--;
                }
            }
            if (x.equals("H")) {
                System.out.println(" This program generates integer random numbers from min to max as you input.");
                System.out.println("Min should not be less then 1, and max not more then 500.");
                System.out.println("If max is less then min, you did something wrong.");
                System.out.println("Generated random never repeat in same session, ");
                System.out.println("so array couldn't be more then distance between min and max");
            }
            if (x.equals("G")) {
                System.out.println(rndm[z]);
            }

        }


    }

}