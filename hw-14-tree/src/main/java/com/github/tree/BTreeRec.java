package com.github.tree;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;

public class BTreeRec implements ITree {

    private Node root;

    private static class Node {

        int value;
        Node right;
        Node left;
        Node parent;

        public Node(int value) {
            this.value = value;
        }

        public void parent(Counter counter, List<Integer> widths) {
        }

        public void reverse() {
        }
    }

    private int size;

    @Override
    public void init(int[] init) {
        if (Objects.isNull(init)) {
            init = new int[0];
        }
        for (int elem : init) {
            add(elem);
        }
    }

    @Override
    public void print() {


    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public int size() {
        return sizeNode(this.root);
    }

    public int sizeNode(Node p) {
        if (p == null) {
            return 0;
        }
        int count = 0;
        count += sizeNode(p.left);
        count++;
        count += sizeNode(p.right);
        return count;
    }

    @Override
    public int[] toArray() {
        if (Objects.isNull(this.root)) {
            return new int[]{};
        }
        int[] array = new int[size()];
        Counter c = new Counter();
        toArrayNode(array, c, this.root);
        return array;
    }

    public void toArrayNode(int[] array, Counter c, Node node) {
        if (Objects.isNull(node)) {
            return;
        }
        toArrayNode(array, c, node.left);
        array[c.index++] = node.value;
        toArrayNode(array, c, node.right);
    }

    @Override
    public void add(int val) {
        if (Objects.isNull(this.root)) {
            this.root = new Node(val);
        }
        addNode(val, this.root);
    }

    private void addNode(int val, Node p) {
        if (val < p.value) {
            if (Objects.isNull(p.left)) {
                p.left = new Node(val);
            } else {
                addNode(val, p.left);
            }
        } else {
            if (Objects.isNull(p.right)) {
                p.right = new Node(val);
            } else {
                addNode(val, p.right);
            }
        }
    }

    @Override
    public void del(int val) {

        Node node = search(val);
        if (Objects.isNull(node.left) && Objects.isNull(node.right)) {
            if (Objects.isNull(node.parent)) {
                this.root = null;
            } else if (Objects.equals(node, node.parent.right)) {
                node.parent.right = null;
            } else {
                node.parent.left = null;
            }
        } else if (Objects.isNull(node.left)) {
            if(Objects.isNull(node.parent)){
                this.root = node.right;
                this.root.parent = null;
            } else if (Objects.equals(node, node.parent.right)) {
                node.parent.right = node.right;
                node.right.parent = node.parent;
            } else {
                node.parent.left = node.right;
                node.right.parent = node.parent;
            }
        } else if (Objects.isNull(node.right)) {
            if(Objects.isNull(node.parent)){
                this.root = node.left;
                this.root.parent = null;
            } else if (Objects.equals(node, node.parent.right)) {
                node.parent.right = node.left;
                node.left.parent = node.parent;
            } else {
                node.parent.left = node.left;
                node.left.parent = node.parent;
            }
        } else {
            Node nodeToSwap = node.right;
            while (Objects.nonNull(nodeToSwap.left)) {
                nodeToSwap = nodeToSwap.left;
            }
            if (Objects.nonNull(nodeToSwap.right)) {
                nodeToSwap.parent.left = nodeToSwap.right;
                nodeToSwap.right.parent = nodeToSwap.parent;
            } else {
                nodeToSwap.parent.left = null;
            }
            node.value = nodeToSwap.value;
        }
        this.size--;
    }

    private Node search(int val) {
        if (this.size == 0) {
            throw new NoSuchElementException();
        }
        return nodeSearch(val, this.root);
    }

    private Node nodeSearch(int value, Node n) {
        if (Objects.isNull(n)) {
            throw new NoSuchElementException();
        }
        if(value == n.value){
            return n;
        } else if(value < n.value){
            return nodeSearch(value, n.left);
        } else { return nodeSearch(value, n.right);
        }
    }

    @Override
    public int getWidth() {
        if(Objects.isNull(this.root)){
            return 0;
        }
        List<Integer> widths = new ArrayList<>();
        Counter counter = new Counter();
        this.root.parent(counter, widths);
        Integer result = widths.get(0);
        for(Integer width : widths){
            if(width > result){
                result = width;
            }
        }
        return result;
    }

    @Override
    public int getHeight(){
        if(Objects.isNull(this.root)){
            return 0;
        }
        Counter currentHeight = new Counter(1);
        Counter maxHeight = new Counter(1);
        this.root.parent(currentHeight, (List<Integer>) maxHeight);
        return maxHeight.index;
    }

    @Override
    public int nodes() {
        return 0;
    }


    @Override
    public int leaves() {
        if(Objects.isNull(this.root)){
            return 0;
        }
        Counter nodeNumber = new Counter(0);
        return nodeNumber.index;
    }

    @Override
    public void reverse() {
        this.root.reverse();
    }

    private static class Counter {

        int index;

        public Counter(int index) {
            this.index = index;
        }

        public Counter() {
            this.index = 0;
        }


    }

}
