package com.guthub.var1.as.factory;

import com.guthub.var1.as.Collision;
import com.guthub.var1.as.Interaction;
import com.guthub.var1.as.InteractionType;
import com.guthub.var1.as.Merge;
import com.guthub.var1.BallManager;
import com.guthub.var1.Ball;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class InteractionFactory {

    public static Interaction getInteraction(InteractionType interactionType, List<Ball> balls, AtomicInteger counter){
        switch (interactionType){
            case MERGE:
                return new Merge(balls, counter);
            case COLLIDE:
                return new Collision(balls, counter);
            default:
                throw new IllegalArgumentException();
        }
    }

}
