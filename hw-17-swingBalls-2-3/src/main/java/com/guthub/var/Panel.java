package com.guthub.var;


import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Panel extends JPanel {

    private AtomicInteger counter = new AtomicInteger(0);
    public static List<Ball> balls = new ArrayList();

    public Panel() {

        setLayout(null);
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                Ball ball = new Ball(e.getPoint());
                add(ball);
                Thread t = new Thread(ball, "Ball-" + counter.incrementAndGet());
                t.start();
                try {
                    balls.add(ball);
                } catch (Exception ea) {
                    ea.printStackTrace();
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

        });
        setVisible(Boolean.TRUE);
    }
}
