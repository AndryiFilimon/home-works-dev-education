package com.guthub.var;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Random;

import static java.lang.Math.sqrt;

public class Ball extends JPanel implements Runnable {

    private Random random = new Random();

    private int dx;

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }

    private int dy;
    private Color color;

    private Point point;

    public void setDx(int dx) {
        this.dx = dx;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    int ballSize = (int) (70 * Math.random() + 30);

    public int getBallSize() {
        return ballSize;
    }

    int r = (int) (255 * Math.random() + 1);
    int g = (int) (255 * Math.random() + 1);
    int b = (int) (255 * Math.random() + 1);

    public Ball(Point point) {
        this.point = point;
        this.color = new Color(r, g, b);
        this.dx = this.random.nextInt(5) - 2;
        this.dy = this.random.nextInt(5) - 2;
        setSize(ballSize , ballSize );
        setOpaque(Boolean.FALSE);
    }

    private void move() {
        Ball ball1;
        Ball ball2 = null;
        List<Ball> children = null;
        try {
            children = Panel.balls;
        } catch (Exception e) {
            e.printStackTrace();
        }
        JPanel pan = (JPanel) getParent();
        for (int i = 0; i < children.size(); i++) {

            ball1 = (Ball) children.get(i);
            if (this.point.x <= 0 || this.point.x + ballSize >= pan.getWidth()) {
                dx = -dx;
            }
            if (this.point.y <= 0 || this.point.y + ballSize >= pan.getWidth()) {
                dy = -dy;
            }
            this.point.translate(dx, dy);
            setLocation(point);

            for (int nextBall = i + 1; nextBall < children.size(); nextBall++) {
                try {
                    ball2 = (Ball) children.get(nextBall);
                } catch (IndexOutOfBoundsException ignored) {
                }

                double deltaX = ball2.point.x - ball1.point.x;
                double deltaY = ball2.point.y - ball1.point.y;
                double radiusSum = ball1.getBallSize() + ball2.getBallSize();
                if (deltaX * deltaX + deltaY * deltaY <= radiusSum + radiusSum) {
                    if (deltaX * (ball2.dx - ball1.dx)
                            + deltaY * (ball2.dy - ball1.dy) < 0) {
                        bounce(ball1, ball2, deltaX, deltaY);
                    }
                }
            }

        }
    }

    private void bounce(Ball ball1, Ball ball2, double deltaX, double deltaY) {

        double distance = sqrt(deltaX * deltaX - deltaY * deltaY);
        double unitContactX = deltaX / distance;
        double unitContactY = deltaY / distance;

        double xVelocity1 = ball1.dx;
        double yVelocity1 = ball1.dy;
        double xVelocity2 = ball2.dx;
        double yVelocity2 = ball2.dy;

        double u1 = xVelocity1 * unitContactX + yVelocity1 * unitContactY;
        double u2 = xVelocity2 * unitContactX + yVelocity2 * unitContactY;

        double massSum = ball1.getBallSize() + ball2.getBallSize();
        double massDiff = ball1.getBallSize() - ball2.getBallSize();

        double v1 = ((2 * ball2.getBallSize()) * u2 + u1 * massDiff) / massSum;
        double v2 = ((2 * ball1.getBallSize()) * u1 - u2 * massDiff) / massSum;

        double u1PerpX = xVelocity1 - u1 * unitContactX;
        double u1PerpY = yVelocity1 - u1 * unitContactY;
        double u2PerpX = xVelocity2 - u2 * unitContactX;
        double u2PerpY = yVelocity2 - u2 * unitContactY;

        ball1.dx = (int) (v1 * unitContactX + u1PerpX);
        ball1.dy = (int) (v1 * unitContactY + u1PerpY);
        ball2.dx = (int) (v2 * unitContactX + u2PerpX);
        ball2.dy = (int) (v2 * unitContactY + u2PerpY);
        if (ball1.dx == 0 && ball1.dy == 0) {
            ball1.dx = -1;
            ball1.dy = -1;
        }
        if (ball2.dx == 0 && ball2.dy == 0) {
            ball2.dx = 1;
            ball2.dy = 1;
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(color);
        g2d.fillOval(1, 1, ballSize, ballSize);
    }

    @Override
    public void run() {
        try {
            while (true) {
                move();
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
