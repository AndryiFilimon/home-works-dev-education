package com.guthub.var;
/**
 * Этот объект фиксирует реакцию на столкновение, если столкновение обнаружено.
 *
 * Время столкновения инициализируется (или сбрасывается) на бесконечность
 * (Float.MAX_VALUE).
 * Методы обнаружения столкновений обнаруживают столкновение в заданном
 * лимит времени. Если столкновение обнаружено в течение установленного времени, они
 * обновляет время столкновения, а также вычисляет и обновляет
 * newSpeedX и newSpeedY. Этот экземпляр не будет отражать столкновение
 * обнаружен сверх установленного времени, что считается бессмысленным.
 *
 * Мы используем время столкновения для вычисления newX и newY. Следовательно, есть
 * нет необходимости хранить newX и newY.
 * Чтобы предотвратить выход за границы, getNewX () и getNewY () вычитают
 * время столкновения по небольшому порогу, вместо возврата
 * точка удара.
 */
public class CollisionResponse {
    // Количества, которые потребуются для дальнейших операций, указываются как двойной;
    //  в противном случае плавают.

    /*Обнаружено время столкновения, сброшено на Float.MAX_VALUE */
    public float t;
    // Порог времени, который нужно вычесть из времени столкновения
    // чтобы предотвратить выход за границу. Предположим, что t <= 1.
    private static final float T_EPSILON = 0.005f;

    /* Расчетная скорость по оси x после столкновения*/
    public float newSpeedX;
    /*Расчетная скорость по оси Y после столкновения */
    public float newSpeedY;

    /*Конструктор, сбрасывающий время столкновения на бесконечность. */
    public CollisionResponse() {
        reset();  // Reset detected collision time to infinity
    }

    /*Сбросить обнаруженное время столкновения до бесконечности. */
    public void reset() {
        this.t = Float.MAX_VALUE;
    }

    /**
     * Скопируйте этот экземпляр в другой, чтобы найти самое раннее столкновение.
     *
     * @param another: экземпляр, в который нужно скопировать.
     */
    public void copy(CollisionResponse another) {
        this.t = another.t;
        this.newSpeedX = another.newSpeedX;
        this.newSpeedY = another.newSpeedY;
    }

    /**
     * Return the x-position after impact.
     *
     * @param currentX : the current x-position.
     * @param speedX : the current x-speed.
     * @return x-position after impact.
     */
    public float getNewX(float currentX, float speedX) {
        //Вычтите небольшой нитью, чтобы убедиться, что она не пересекает границу.
        if (t > T_EPSILON) {
            return (float)(currentX + speedX * (t - T_EPSILON));
        } else {
            return currentX;
        }
    }

    /**
     * Верните Y-позицию после удара.
     *
     * @param currentY: текущая x-позиция.
     * @param speedY: текущая x-скорость.
     * @ вернуться в положение по оси Y после удара.
     */
    public float getNewY(float currentY, float speedY) {
        // Subtract by a small thread to make sure that it does not cross the bound.
        if (t > T_EPSILON) {
            return (float)(currentY + speedY * (t - T_EPSILON));
        } else {
            return currentY;
        }
    }

    /**
     * Return the precise x-position of the point of impact.
     * Needed in some collision detection operations.
     *
     * @param currentX : the current x-position.
     * @param speedX : the current x-speed.
     * @return x-position of the point of impact.
     */
    public double getImpactX(float currentX, float speedX) {
        return currentX + speedX * t;
    }

  /**
   * Верните точное y-положение точки удара.
   * Требуется в некоторых операциях по обнаружению столкновений.
   *
   * @param currentY: текущая x-позиция.
   * @param speedY: текущая x-скорость.
   * @return y-позиция точки удара.
   */
    public double getImpactY(float currentY, float speedY) {
        return currentY + speedY * t;
    }

}
