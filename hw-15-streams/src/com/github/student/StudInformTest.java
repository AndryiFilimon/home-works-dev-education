package com.github.student;

import jdk.internal.access.JavaIOFileDescriptorAccess;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static com.github.student.People.students;
import static com.github.student.StudInform.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
//import static com.github.student.StudInform.studF;


public class StudInformTest {

    @Before
    public void setUp(){
        People.list();
    }

//--------------------------------------------------------------
//                          studF
// -------------------------------------------------------------

    @Test
    public void studFMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(3, "Vano", "Rulack", 2030,
                "Kyrivograd", "Xiaomi", "Bionic", "2", "1"));
        exp.add(new Student(5, "Warn", "Mul", 2034,
                "Kharkov", "Honor", "Bionic", "4", "2"));
        List<Student> act = studF("Bionic");
        assertEquals(exp, act);
    }

    @Test
    public void studFTwo() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Koliya", "Boiko", 2033,
                "Kiev", "Samsung", "IT-Proger", "3", "2"));
        exp.add(new Student(2, "Kuniya", "Zoiko", 2029,
                "Kiev", "LG", "IT-Proger", "1", "3"));
        List<Student> act = studF("IT-Proger");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studFNull() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = studF(null);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studFEmpty() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = studF("");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studFException() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = studF("Banana");
        Assert.assertEquals(act, exp);
    }

//--------------------------------------------------------------
//                     studFucAndYea
// -------------------------------------------------------------

    @Test
    public void studFucAndYearTest() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(3, "Vano", "Rulack", 2030,
                "Kyrivograd", "Xiaomi", "Bionic", "2", "1"));
        List<Student> act = studFucAndYear("Bionic", "2");
        assertEquals(exp, act);
    }

    @Test
    public void studFucAndYearTwo() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(6, "Jesika", "Nuts", 2033, "Varburg", "Iphone", "Solder", "2", "2"));
        exp.add(new Student(7, "Jesi", "Nut", 2032, "Varbu", "Iphone", "Solder", "2", "1"));
        List<Student> act = studFucAndYear("Solder", "2");
        assertEquals(exp, act);
    }

    @Test
    public void studFucAndYearNull() {
        List<Student> act = studFucAndYear(null, "2");
        Assert.assertNull(act);
    }

    @Test
    public void studFucAndYearEmpty() {
        List<Student> act = studFucAndYear("", "2");
        Assert.assertNull(act);
    }

    @Test
    public void studFucAndYearNullTwo() {
        List<Student> act = studFucAndYear("Solder", null);
        Assert.assertNull(act);
    }

    @Test
    public void studFucAndYearEmptyTwo() {
        List<Student> act = studFucAndYear("Solder", "");
        Assert.assertNull(act);
    }


//    ----------------------------------------------------------
//                     studYear
// -------------------------------------------------------------

    @Test
    public void studYearMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Koliya", "Boiko", 2033, "Kiev", "Samsung", "IT-Proger", "3", "2"));
        exp.add(new Student(4, "Andy", "Kazak", 2031, "Varburg", "Iphone", "Solder", "3", "2"));
        exp.add(new Student(5, "Warn", "Mul", 2034, "Kharkov", "Honor", "Bionic", "4", "2"));
        exp.add(new Student(6, "Jesika", "Nuts", 2033, "Varburg", "Iphone", "Solder", "2", "2"));
        exp.add(new Student(7, "Jesi", "Nut", 2032, "Varbu", "Iphone", "Solder", "2", "1"));
        List<Student> act = studYear(2030);
        assertEquals(exp, act);
    }

    @Test
    public void studYearOne() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(5, "Warn", "Mul", 2034, "Kharkov", "Honor", "Bionic", "4", "2"));
        List<Student> act = studYear(2033);
        assertEquals(exp, act);
    }

    @Test
    public void studYearMore() {
        List<Student> exp = new ArrayList<>();
        List<Student> act = studYear(3000);
        assertEquals(exp, act);
    }


    @Test
    public void studYearOdd() {
        List<Student> act = studYear(-2);
        Assert.assertNull(act);
    }

    @Test
    public void studYearZero() {
        List<Student> act = studYear(0);
        Assert.assertNull(act);
    }

//--------------------------------------------------------------
//                     studYearAndYear
// -------------------------------------------------------------

    @Test
    public void studYearAndYearOne() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(5, "Warn", "Mul", 2034, "Kharkov", "Honor", "Bionic", "4", "2"));
        List<Student> act = studYearAndYear(2033);
        assertEquals(exp, act);
    }

    @Test
    public void studYearAndYearTwo() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Koliya", "Boiko", 2033, "Kiev", "Samsung", "IT-Proger", "3", "2"));
        List<Student> act = studYearAndYear(2032);
        assertEquals(exp, act);
    }

    @Test
    public void studYearAndYearThree() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Koliya", "Boiko", 2033, "Kiev", "Samsung", "IT-Proger", "3", "2"));
        List<Student> act = studYearAndYear(1);
        assertEquals(exp, act);
    }

    @Test
    public void studYearAndYeaOdd() {
        List<Student> act = studYearAndYear(-2);
        Assert.assertNull(act);
    }

    @Test
    public void studYearAndYearZero() {
        List<Student> act = studYearAndYear(0);
        assertNull(act);
    }

//--------------------------------------------------------------
//                        studGroup
// -------------------------------------------------------------

    @Test
    public void studGroupMany() {
        List<String> exp = new ArrayList<>();
        exp.add("Koliya Boiko");
        exp.add("Andy Kazak");
        exp.add("Warn Mul");
        exp.add("Jesika Nuts");
        List<Student> act = studGroup("2");
        assertEquals(exp, act);
    }

    @Test
    public void studGroupOne() {
        List<String> exp = new ArrayList<>();
        exp.add("Kuniya Zoiko");
        List<Student> act = studGroup("3");
        assertEquals(exp, act);
    }

    @Test
    public void studGroupNull() {
        List<Student> act = studGroup(null);
        Assert.assertNull(act);
    }

    @Test
    public void studGroupEmpty() {
        List<Student> act = studGroup("");
        Assert.assertNull(act);
    }

//--------------------------------------------------------------
//                        studCount
// -------------------------------------------------------------


    @Test
    public void studCountTest() {
        long exp = 2;
        long act = studCount("IT-Proger");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studCountTwo() {
        long exp = 3;
        long act = studCount("Solder");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studCountThree() {
        long exp = 0;
        long act = studCount("Banana");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studCountZero() {
        long exp = 0L;
        long act = studCount(null);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studCountEmpty() {
        long exp = 0;
        long act = studCount("");
        Assert.assertEquals(act, exp);
    }

//--------------------------------------------------------------
//                        studFind
// -------------------------------------------------------------

    @Test
    public void studFindTest() {
        long exp = 1;
        long act = studFind("IT-Proger");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studFindTwo() {
        long exp = 0;
        long act = studFind("Banana");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studFindThree() {
        long exp = 1;
        long act = studFind("Solder");
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studFindZero() {
        long exp = 0;
        long act = studCount(null);
        Assert.assertEquals(act, exp);
    }

    @Test
    public void studFindEmpty() {
        long exp = 0;
        long act = studCount("");
        Assert.assertEquals(act, exp);
    }

//--------------------------------------------------------------
//                        groupStudGroup
// -------------------------------------------------------------


    @Test
    public void groupStudGroupMany() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Koliya", "Boiko", 2033, "Kiev", "Samsung", "IT-Proger", "3", "3"));
        exp.add(new Student(2, "Kuniya", "Zoiko", 2029, "Kiev", "LG", "IT-Proger", "1", "3"));
        exp.add(new Student(3, "Vano", "Rulack", 2030, "Kyrivograd", "Xiaomi", "Bionic", "2", "3"));
        exp.add(new Student(4, "Andy", "Kazak", 2031, "Varburg", "Iphone", "Solder", "3", "3"));
        exp.add(new Student(5, "Warn", "Mul", 2034, "Kharkov", "Honor", "Bionic", "4", "3"));
        exp.add(new Student(6, "Jesika", "Nuts", 2033, "Varburg", "Iphone", "Solder", "2", "3"));
        exp.add(new Student(7, "Jesi", "Nut", 2032, "Varbu", "Iphone", "Solder", "2", "3"));
        List<Student> act = groupStudGroup("3");
        assertEquals(exp, act);
    }

    @Test
    public void groupStudGroupNull() {
        List<Student> act = groupStudGroup(null);
        Assert.assertNull(act);
    }

    @Test
    public void groupStudGroupEmpty() {
        List<Student> act = groupStudGroup("");
        Assert.assertNull(act);
    }

//--------------------------------------------------------------
//                        facStudFac
// -------------------------------------------------------------

    @Test
    public void facStudFacTest() {
        List<Student> exp = new ArrayList<>();
        exp.add(new Student(1, "Koliya", "Boiko", 2033, "Kiev", "Samsung", "IT-Proger", "3", "2"));
        exp.add(new Student(2, "Kuniya", "Zoiko", 2029, "Kiev", "LG", "IT-Proger", "1", "3"));
        exp.add(new Student(3, "Vano", "Rulack", 2030, "Kyrivograd", "Xiaomi", "IT-Proger", "2", "1"));
        exp.add(new Student(4, "Andy", "Kazak", 2031, "Varburg", "Iphone", "IT-Proger", "3", "2"));
        exp.add(new Student(5, "Warn", "Mul", 2034, "Kharkov", "Honor", "IT-Proger", "4", "2"));
        exp.add(new Student(6, "Jesika", "Nuts", 2033, "Varburg", "Iphone", "IT-Proger", "2", "2"));
        exp.add(new Student(7, "Jesi", "Nut", 2032, "Varbu", "Iphone", "IT-Proger", "2", "1"));
        List<Student> act = facStudFac("IT-Proger");
        assertEquals(exp, act);
    }

    @Test
    public void facStudFacNull() {
        List<Student> act = facStudFac(null);
        Assert.assertNull(act);
    }

    @Test
    public void facStudFacEmpty() {
        List<Student> act = facStudFac("");
        Assert.assertNull(act);
    }

//--------------------------------------------------------------
//                        methodReduce
// -------------------------------------------------------------

    @Test
    public void methodReduceTest() {
        String exp = "Koliya Boiko - IT-Proger 2\n" +
                "Kuniya Zoiko - IT-Proger 3\n" +
                "Vano Rulack - Bionic 1\n" +
                "Andy Kazak - Solder 2\n" +
                "Warn Mul - Bionic 2\n" +
                "Jesika Nuts - Solder 2\n" +
                "Jesi Nut - Solder 1\n";
        String act = methodReduce();
        assertEquals(exp, act);
    }

//--------------------------------------------------------------
//                        sortFaculty
// -------------------------------------------------------------

    @Test
    public void sortFacultyTest() {
        Map<String, List<Student>> exp = new HashMap<>();
        List<Student> solder = new ArrayList<>();
        solder.add(new Student(4, "Andy", "Kazak", 2031, "Varburg", "Iphone", "Solder", "3", "2"));
        solder.add(new Student(6, "Jesika", "Nuts", 2033, "Varburg", "Iphone", "Solder", "2", "2"));
        solder.add(new Student(7, "Jesi", "Nut", 2032, "Varbu", "Iphone", "Solder", "2", "1"));
        List<Student> itProger = new ArrayList<>();
        itProger.add(new Student(1, "Koliya", "Boiko", 2033, "Kiev", "Samsung", "IT-Proger", "3", "2"));
        itProger.add(new Student(2, "Kuniya", "Zoiko", 2029, "Kiev", "LG", "IT-Proger", "1", "3"));
        List<Student> bionic = new ArrayList<>();
        bionic.add(new Student(3, "Vano", "Rulack", 2030, "Kyrivograd", "Xiaomi", "Bionic", "2", "1"));
        bionic.add(new Student(5, "Warn", "Mul", 2034, "Kharkov", "Honor", "Bionic", "4", "2"));
        exp.put("Solder", solder);
        exp.put("IT-Proger", itProger);
        exp.put("Bionic", bionic);
        Map<String, List<Student>> act = sortFaculty();
        assertEquals(exp, act);
    }

//--------------------------------------------------------------
//                        sortCourse
// -------------------------------------------------------------

    @Test
    public void sortCourseTest() {
        Map<String, List<Student>> exp = new HashMap<>();
        List<Student> one = new ArrayList<>();
        one.add(new Student(2, "Kuniya", "Zoiko", 2029, "Kiev", "LG", "IT-Proger", "1", "3"));
        List<Student> two = new ArrayList<>();
        two.add(new Student(3, "Vano", "Rulack", 2030, "Kyrivograd", "Xiaomi", "Bionic", "2", "1"));
        two.add(new Student(6, "Jesika", "Nuts", 2033, "Varburg", "Iphone", "Solder", "2", "2"));
        two.add(new Student(7, "Jesi", "Nut", 2032, "Varbu", "Iphone", "Solder", "2", "1"));
        List<Student> three = new ArrayList<>();
        three.add(new Student(1, "Koliya", "Boiko", 2033, "Kiev", "Samsung", "IT-Proger", "3", "2"));
        three.add(new Student(4, "Andy", "Kazak", 2031, "Varburg", "Iphone", "Solder", "3", "2"));
        List<Student> four = new ArrayList<>();
        four.add(new Student(5, "Warn", "Mul", 2034, "Kharkov", "Honor", "Bionic", "4", "2"));
        exp.put("1", one);
        exp.put("2", two);
        exp.put("3", three);
        exp.put("4", four);
        Map<String, List<Student>> act = sortCourse();
        assertEquals(exp, act);
    }

//--------------------------------------------------------------
//                        sortGroup
// -------------------------------------------------------------

    @Test
    public void sortGroupTest() {
        Map<String, List<Student>> exp = new HashMap<>();
        List<Student> one = new ArrayList<>();
        one.add(new Student(3, "Vano", "Rulack", 2030, "Kyrivograd", "Xiaomi", "Bionic", "2", "1"));
        one.add(new Student(7, "Jesi", "Nut", 2032, "Varbu", "Iphone", "Solder", "2", "1"));
        List<Student> two = new ArrayList<>();
        two.add(new Student(1, "Koliya", "Boiko", 2033, "Kiev", "Samsung", "IT-Proger", "3", "2"));
        two.add(new Student(4, "Andy", "Kazak", 2031, "Varburg", "Iphone", "Solder", "3", "2"));
        two.add(new Student(5, "Warn", "Mul", 2034, "Kharkov", "Honor", "Bionic", "4", "2"));
        two.add(new Student(6, "Jesika", "Nuts", 2033, "Varburg", "Iphone", "Solder", "2", "2"));
        List<Student> three = new ArrayList<>();
        three.add(new Student(2, "Kuniya", "Zoiko", 2029, "Kiev", "LG", "IT-Proger", "1", "3"));
        exp.put("1", one);
        exp.put("2", two);
        exp.put("3", three);
        Map<String, List<Student>> act = sortGroup();
        assertEquals(exp, act);
    }

//--------------------------------------------------------------
//                        studAllFac
// -------------------------------------------------------------

    @Test
    public void studAllFacTest() {
        boolean exp = false;
        boolean act = studAllFac("Bionic");
        assertEquals(exp, act);
    }

    @Test
    public void studAllFacNull() {
        boolean exp = false;
        boolean act = studAllFac(null);
        Assert.assertEquals(act,exp);
    }

    @Test
    public void studAllFacEmpty() {
        boolean exp = false;
        boolean act = studAllFac("");
        Assert.assertEquals(act, exp);
    }

//--------------------------------------------------------------
//                        studAllFac
// -------------------------------------------------------------

    @Test
    public void studOneFacTest() {
        boolean exp = true;
        boolean act = studOneFac("Bionic");
        assertEquals(exp, act);
    }

    @Test
    public void studOneFacNull() {
        boolean exp = false;
        boolean act = studOneFac(null);
        Assert.assertEquals(act,exp);
    }

    @Test
    public void studOneFacEmpty() {
        boolean exp = false;
        boolean act = studOneFac("");
        Assert.assertEquals(act, exp);
    }

//--------------------------------------------------------------
//                        studAllGroup
// -------------------------------------------------------------

    @Test
    public void studAllGroupTest() {
        boolean exp = false;
        boolean act = studAllGroup("2");
        assertEquals(exp, act);
    }

    @Test
    public void studAllGroupNull() {
        boolean exp = false;
        boolean act = studAllGroup(null);
        Assert.assertEquals(act,exp);
    }

    @Test
    public void studAllGroupEmpty() {
        boolean exp = false;
        boolean act = studAllGroup("");
        Assert.assertEquals(act, exp);
    }

//--------------------------------------------------------------
//                        studOneGroup
// -------------------------------------------------------------

    @Test
    public void studOneGroupTest() {
        boolean exp = true;
        boolean act = studOneGroup("2");
        assertEquals(exp, act);
    }

    @Test
    public void studOneGroupNull() {
        boolean exp = false;
        boolean act = studOneGroup(null);
        Assert.assertEquals(act,exp);
    }

    @Test
    public void studOneGroupEmpty() {
        boolean exp = false;
        boolean act = studOneGroup("");
        Assert.assertEquals(act, exp);
    }
}