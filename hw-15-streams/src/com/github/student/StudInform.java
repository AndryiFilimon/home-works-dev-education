package com.github.student;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.github.student.People.students;

public class StudInform {

    public static List studF(String faculty) {
        return students.stream().filter(f -> f.getFaculty().equals(faculty))
                .collect(Collectors.toList());
    }

    public static List studFucAndYear(String faculty, String course) {
        if (faculty == null || course == null) {
            return null;
        } else if (faculty == "" || course == "") {
            return null;
        }
        return students.stream()
                .filter(f -> f.getFaculty().equals(faculty))
                .filter(f -> f.getCourse().equals(course))
                .collect(Collectors.toList());
    }

    public static List studYear(int y) {
        if (y <= 0) {
            return null;
        }
        return students.stream()
                .filter(f -> f.getYearOfBirth() > y)
                .collect(Collectors.toList());
    }

    public static List studYearAndYear(int firstYear) {
        if (firstYear <= 0) {
            return null;
        }
        return students.stream()
                .filter(f -> f.getYearOfBirth() > firstYear)
                .findFirst()
                .stream().collect(Collectors.toList());
    }

    public static List studGroup(String group) {
        if(group == null || group == ""){
            return null;
        }
        return students.stream()
                .filter(f -> f.getGroup().equals(group))
                .flatMap(s -> Stream.of(s.getFirstName() + " " + s.getLastName()))
                .collect(Collectors.toList());
    }

    public static long studCount(String faculty) {
        return students.stream()
                .filter(f -> f.getFaculty().equals(faculty)).count();
    }

    public static long studFind(String faculty) {
        return students.stream()
                .filter(f -> f.getFaculty().equals(faculty))
                .findAny()
                .stream().count();
    }

    public static List<Student> groupStudGroup(String group) {
        if(group == null || group == ""){
            return null;
        }
        return students.stream()
                .peek(s -> {
                    if (s.getGroup().equals(s.getGroup())) {
                        s.setGroup(group);
                    }
                })
                .collect(Collectors.toList());
    }

    public static List facStudFac(String faculty) {
        if(faculty == null || faculty == ""){
            return null;
        }
        return students.stream()
                .peek(s -> {
                    if (s.getFaculty().equals(s.getFaculty())) {
                        s.setFaculty(faculty);
                    }
                })
                .collect(Collectors.toList());
    }

    public static String methodReduce() {
        return students.stream()
                .flatMap(s -> Stream.of(s.getFirstName() + " " + s.getLastName() + " - "
                        + s.getFaculty() + " " + s.getGroup() + "\n"))
                .reduce("", (partial, elem) -> partial + elem);
    }

    public static Map<String, List<Student>> sortFaculty() {
        Map<String, List<Student>> resultMap = students.stream()
                .collect(Collectors.groupingBy(Student::getFaculty));
        return resultMap;
    }

    public static Map<String, List<Student>> sortCourse() {
        Map<String, List<Student>> mapCourse = students.stream()
                .collect(Collectors.groupingBy(Student::getCourse));
        return mapCourse;
    }

    public static Map<String, List<Student>> sortGroup() {
        Map<String, List<Student>> mapGroup = students.stream()
                .collect(Collectors.groupingBy(Student::getGroup));
        return mapGroup;
    }

    public static boolean studAllFac(String faculty) {
        return students.stream()
                .allMatch(f -> f.getFaculty().equals(faculty));
    }

    public static boolean studOneFac(String faculty) {
        return students.stream()
                .anyMatch(f -> f.getFaculty().equals(faculty));
    }

    public static boolean studAllGroup(String group) {
        return students.stream()
                .allMatch(g -> g.getGroup().equals(group));
    }

    public static boolean studOneGroup(String group) {
        return students.stream()
                .anyMatch(g -> g.getGroup().equals(group));
    }
}