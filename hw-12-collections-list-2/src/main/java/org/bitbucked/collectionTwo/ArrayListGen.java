package main.java.org.bitbucked.collectionTwo;

import org.bitbucked.collectionOne.exception.ListEmptyException;

public class ArrayListGen<T extends Comparable<? super T>> implements IlistGeneric<T>  {

        private T[] array = (T[]) new Comparable[10];

        private int index;

        public int resize(int oldSize){
            int newSize = (int) (oldSize * 1.3);
            this.array = (T[]) new Comparable[newSize];
            return newSize;
        }

        @Override
        public void init(T[] init) {
            if (init == null) {
                this.index = 0;
            } else {
                this.index = init.length;
                if (this.index > this.array.length) {
                    resize(this.index);
                }
                for (int i = 0; i < this.index; i++) {
                    this.array[i] = init[i];
                }
            }
        }


        @Override
        public void clear() {
            this.index = 0;
        }

        @Override
        public int size() {
            return this.index;
        }

        @Override
        public T[] toArray() {
            T[] result = (T[]) new Comparable[this.index];
            for (int i = 0; i < this.index; i++) {
                result[i] = array[i];
            }
            return result;
        }

        @Override
        public void addStart(T val) {
            int size = size();
            T[] tmp = (T[]) new Comparable[size + 1];
            if (size == 0) {
                tmp[0] = val;
            } else {
                for (int i = this.index; i > 0; i--) {
                    tmp[i] = this.array[i - 1];
                }
                tmp[0] = val;
            }
            this.index++;
            this.array = tmp;
        }

        @Override
        public void addStart(T val, Comparable<?> com) {

        }

        @Override
        public void addEnd(T val) {
            int size = size();
            T[] tmp = (T[]) new Comparable[size + 1];
            if (size == 0) {
                tmp[0] = val;
            } else {
                for (int i = 0; i < size; i++) {
                    tmp[i] = this.array[i];
                }
                tmp[size] = val;
            }
            this.index++;
            this.array = tmp;
        }

        @Override
        public void addByPos(int pos, T val) {
            if (this.index == 0) {
                throw new ListEmptyException();
            }
            this.index++;
            T[] tmp = (T[]) new Comparable[this.index];
            for (int i = 0; i < pos; i++) {
                tmp[i] = this.array[i];
            }
            tmp[pos] = val;
            for (int j = pos+1; j < this.index; j++){
                tmp[j] = this.array[j-1];
            }
            this.array = tmp;
        }

        @Override
        public T removeStart() {
            if (this.index == 0) {
                throw new ListEmptyException();
            }
            this.index--;
            T[] tmp = (T[]) new Comparable[this.index];
            T remove = this.array[0];
            for (int i = 0; i < this.index; i++) {
                tmp[i] = this.array[i+1];
            }
            this.array = tmp;
            return remove;
        }

        @Override
        public T removeEnd() {
            if (this.index == 0) {
                throw new ListEmptyException();
            }
            this.index--;
            T[] tmp = (T[]) new Comparable[this.index];
            T remove = this.array[this.index];
            for (int i = 0; i < this.index; i++) {
                tmp[i] = this.array[i];
            }
            this.array = tmp;
            return remove;
        }

        @Override
        public T removeByPos(int pos) {
            if (this.index == 0) {
                throw new ListEmptyException();
            }
            this.index--;
            T[] tmp = (T[]) new Comparable[this.index];
            T remove = this.array[pos];
            for (int i = 0; i < this.index; i++) {
                if(pos <= i){
                    tmp[i] = this.array[i+1];
                } else {
                    tmp[i] = this.array[i];
                }
            }
            this.array = tmp;
            return remove;
        }

        @Override
        public T max() {
            int max;
            int maxPos;
            int count;
            if (this.index == 0) {
                throw new ListEmptyException();
            } else if(this.index == 1){
                maxPos = 0;
            } else {
                max = this.array[0].compareTo(this.array[1]);
                count = 0;
                maxPos = count;
                for (int i = 1; i < this.index-1; i++){
                    count++;
                    if (this.array[i].compareTo(this.array[i+1]) >= max){
                        maxPos = count;
                        max = this.array[i].compareTo(this.array[i+1]);
                    }
                }
                if(this.array[this.index-1].compareTo(this.array[0]) >= max){
                    count++;
                    maxPos = count;
                }
            }
            return get(maxPos);
        }

        @Override
        public T min() {
            int min;
            int minPos;
            int count;
            if (this.index == 0) {
                throw new ListEmptyException();
            } else if(this.index == 1){
                minPos = 0;
            } else {
                min = this.array[0].compareTo(this.array[1]);
                count = 0;
                minPos = count;
                for (int i = 1; i < this.index-1; i++){
                    count++;
                    if (this.array[i].compareTo(this.array[i+1]) <= min){
                        minPos = count;
                        min = this.array[i].compareTo(this.array[i+1]);
                    }
                }
                if(this.array[this.index-1].compareTo(this.array[0]) <= min){
                    count++;
                    minPos = count;
                }
            }
            return get(minPos);
        }

        @Override
        public int maxPos() {
            int max;
            int maxPos;
            int count;
            if (this.index == 0) {
                throw new ListEmptyException();
            } else if(this.index == 1){
                maxPos = 0;
            } else {
                max = this.array[0].compareTo(this.array[1]);
                count = 0;
                maxPos = count;
                for (int i = 1; i < this.index-1; i++){
                    count++;
                    if (this.array[i].compareTo(this.array[i+1]) >= max){
                        maxPos = count;
                        max = this.array[i].compareTo(this.array[i+1]);
                    }
                }
                if(this.array[this.index-1].compareTo(this.array[0]) >= max){
                    count++;
                    maxPos = count;
                }
            }
            return maxPos;
        }

        @Override
        public int minPos() {
            int min;
            int minPos;
            int count;
            if (this.index == 0) {
                throw new ListEmptyException();
            } else if(this.index == 1){
                minPos = 0;
            } else {
                min = this.array[0].compareTo(this.array[1]);
                count = 0;
                minPos = count;
                for (int i = 1; i < this.index-1; i++){
                    count++;
                    if (this.array[i].compareTo(this.array[i+1]) <= min){
                        minPos = count;
                        min = this.array[i].compareTo(this.array[i+1]);
                    }
                }
                if(this.array[this.index-1].compareTo(this.array[0]) <= min){
                    count++;
                    minPos = count;
                }
            }
            return minPos;
        }

        @Override
        public T[] sort() {
            int size = size ();
            if (size == 0) {
                throw new ListEmptyException();
            } else if (size == 1){
                return this.array;
            } else {
                int max = this.array[0].compareTo(this.array[1]);
                for (int i = 1; i < size - 1; i++) {
                    for (int j = 0; j < size - i - 2; j++) {
                        if (this.array[j].compareTo(this.array[j + 1]) >= max) {
                            T temp = this.array[j];
                            this.array[j] = this.array[j + 1];
                            this.array[j + 1] = temp;
                            max = this.array[j].compareTo(this.array[j + 1]);
                        }
                    }
                }
                if (this.array[size - 1].compareTo(this.array[0]) >= max) {
                    T temp = this.array[size - 1];
                    this.array[size - 1] = this.array[0];
                    this.array[0] = temp;
                }
                return this.array;
            }
        }

        @Override
        public T get(int pos) {
            if (this.index == 0) {
                throw new ListEmptyException();
            }
            return this.array[pos];
        }

        @Override
        public T[] halfRevers() {
            if (this.index == 0) {
                throw new ListEmptyException();
            }
            T[] result = (T[]) new Comparable[this.index];
            int half;
            if(this.index%2 == 0){
                half = this.index/2;
            } else {
                half = (this.index+1)/2;
            }
            for (int i = half; i < this.index; i++){
                result[i-half] = this.array[i];
            }
            for (int j = 0; j < half; j++){
                result [j+this.index-half] = this.array[j];
            }
            return result;
        }

        @Override
        public T[] revers() {
            if (this.index == 0) {
                throw new ListEmptyException();
            }
            T[] result = (T[]) new Comparable[this.index];
            for (int i = 0; i < this.index; i++){
                result[i] = this.array[this.index-1-i];
            }
            return result;
        }

        @Override
        public void set(int pos, T val) {
            if (this.index == 0) {
                throw new ListEmptyException();
            }
            T[] tmp = (T[]) new Comparable[this.index];
            for (int i = 0; i < this.index; i++) {
                if (pos == i){
                    tmp[i] = val;
                } else {
                    tmp[i] = this.array[i];
                }
            }
            this.array = tmp;
        }
    }
