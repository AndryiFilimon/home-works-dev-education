package main.java.org.bitbucked.collectionTwo;

import org.bitbucked.collectionOne.exception.ListEmptyException;
import org.junit.Assert;
import org.junit.Test;

public class ArrayListGenericTest {

        private final IlistGeneric<String> list = new ArrayListGen<>();

        //=================================================
        //=================== Clean =======================
        //=================================================

        @Test
        public void clearMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.init(array);
            this.list.clear();
            String[] exp = {};
            Object[] act = this.list.toArray();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void clearTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            this.list.clear();
            String[] exp = {};
            Object[] act = this.list.toArray();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void clearOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            this.list.clear();
            String[] exp = {};
            Object[] act = this.list.toArray();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void clearZero() {
            String[] array = new String[]{};
            this.list.init(array);
            this.list.clear();
            String[] exp = {};
            Object[] act = this.list.toArray();
            Assert.assertArrayEquals(exp, act);
        }

        //=================================================
        //=================== Size ========================
        //=================================================

        @Test
        public void sizeMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.init(array);
            int exp = 10;
            int act = this.list.size();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void sizeTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            int exp = 2;
            int act = this.list.size();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void sizeOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            int exp = 1;
            int act = this.list.size();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void sizeZero() {
            String[] array = new String[]{};
            this.list.init(array);
            int exp = 0;
            int act = this.list.size();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        //=================================================
        //=================== Add Start ===================
        //=================================================

        @Test
        public void addStartMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.init(array);
            String[] exp = new String[]{"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.addStart("Zero");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addStartMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four"};
            this.list.init(array);
            String[] exp = new String[]{"Zero", "One", "Two", "Three", "Four"};
            this.list.addStart("Zero");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addStartTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String[] exp = new String[]{"Zero", "One", "Two"};
            this.list.addStart("Zero");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }
        @Test
        public void addStartOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            String[] exp = new String[]{"Zero", "One"};
            this.list.addStart("Zero");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addStartZero() {
            String[] array = new String[]{};
            this.list.init(array);
            String[] exp = new String[]{"Zero"};
            this.list.addStart("Zero");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        //=================================================
        //=================== Add End =====================
        //=================================================

        @Test
        public void addEndMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven"};
            this.list.addEnd("Eleven");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addEndMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two", "Three", "Four", "Five", "Six"};
            this.list.addEnd("Six");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addEndTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two", "Three"};
            this.list.addEnd("Three");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }
        @Test
        public void addEndOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two"};
            this.list.addEnd("Two");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addEndZero() {
            String[] array = new String[]{};
            this.list.init(array);
            String[] exp = new String[]{"One"};
            this.list.addEnd("One");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        //=================================================
        //=================== Add by Pos ==================
        //=================================================

        @Test
        public void addByPosMany() {
            String[] array = new String[]{"One", "Two", "Three", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.addByPos(3,"Four");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addByPosMany2() {
            String[] array = new String[]{"One", "Three", "Four", "Five", "Six"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two", "Three", "Four", "Five", "Six"};
            this.list.addByPos(1, "Two");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addByPosManyStart() {
            String[] array = new String[]{"Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.addByPos(0, "One");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addByPosManyEnd() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Ten"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.addByPos(8, "Nine");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addByPosTwo() {
            String[] array = new String[]{"One", "Three"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two", "Three"};
            this.list.addByPos(1,"Two");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }
        @Test
        public void addByPosOne() {
            String[] array = new String[]{"Two"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two"};
            this.list.addByPos(0,"One");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void addByPosOne2() {
            String[] array = new String[]{"Two"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two"};
            this.list.addByPos(1,"One");
            Object[] act = this.list.toArray();
            this.list.clear();
        }

        @Test (expected = ListEmptyException.class)
        public void addByPosZero() {
            String[] array = new String[]{};
            this.list.init(array);
            this.list.addByPos(2, "Eleven");
            Object[] act = this.list.toArray();
            this.list.clear();
        }

        //=================================================
        //=================== Remove Start ================
        //=================================================

        @Test
        public void removeStartMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.init(array);
            String expNum = "One";
            String[] exp = new String[]{"Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            String actNum = this.list.removeStart();
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void removeStartMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five"};
            this.list.init(array);
            String expNum = "One";
            String[] exp = new String[]{"Two", "Three", "Four", "Five"};
            String actNum = this.list.removeStart();
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void removeStartTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String expNum = "One";
            String[] exp = new String[]{"Two"};
            String actNum = this.list.removeStart();
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }
        @Test
        public void removeStartOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            String expNum = "One";
            String[] exp = new String[]{};
            String actNum = this.list.removeStart();
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void removeStartZero() {
            String[] array = new String[]{};
            this.list.init(array);
            String actNum = this.list.removeStart();
            Object[] act = this.list.toArray();
            this.list.clear();
        }

        //=================================================
        //=================== remove End ==================
        //=================================================

        @Test
        public void removeEndMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.init(array);
            String expNum = "Ten";
            String[] exp = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            String actNum = this.list.removeEnd();
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void removeEndMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five"};
            this.list.init(array);
            String expNum = "Five";
            String[] exp = new String[]{"One", "Two", "Three", "Four"};
            String actNum = this.list.removeEnd();
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void removeEndTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String expNum = "Two";
            String[] exp = new String[]{"One"};
            String actNum = this.list.removeEnd();
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }
        @Test
        public void removeEndOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            String expNum = "One";
            String[] exp = new String[]{};
            String actNum = this.list.removeEnd();
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void removeEndZero() {
            String[] array = new String[]{};
            this.list.init(array);
            String actNum = this.list.removeEnd();
            Object[] act = this.list.toArray();
            this.list.clear();
        }

        //=================================================
        //=================== Remove by Pos ===============
        //=================================================

        @Test
        public void removeByPosMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            String expNum = "Six";
            String[] exp = new String[]{"One", "Two", "Three", "Four", "Five", "Seven", "Eight", "Nine"};
            String actNum = this.list.removeByPos(5);
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void removeByPosMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five"};
            this.list.init(array);
            String expNum = "Two";
            String[] exp = new String[]{"One", "Three", "Four", "Five"};
            String actNum = this.list.removeByPos(1);
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void removeByPosManyEnd() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};
            this.list.init(array);
            String expNum = "Ten";
            String[] exp = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            String actNum = this.list.removeByPos(9);
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void removeByPosManyStart() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            String expNum = "One";
            String[] exp = new String[]{"Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            String actNum = this.list.removeByPos(0);
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void removeByPosTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String expNum = "Two";
            String[] exp = new String[]{"One"};
            String actNum = this.list.removeByPos(1);
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }
        @Test
        public void removeByPosOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            String expNum = "One";
            String[] exp = new String[]{};
            String actNum = this.list.removeEnd();
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertEquals(expNum, actNum);
            Assert.assertArrayEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void removeByPosZero() {
            String[] array = new String[]{};
            this.list.init(array);
            String actNum = this.list.removeByPos(1);
            Object[] act = this.list.toArray();
            this.list.clear();
        }

        //=================================================
        //=================== Get ========================
        //=================================================

        @Test
        public void getMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            String exp= "Six";
            String act = this.list.get(5);
            this.list.clear();
            Assert.assertEquals(exp, act);
        }


        @Test
        public void getMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four"};
            this.list.init(array);
            String exp= "Two";
            String act = this.list.get(1);
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void getTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String exp= "One";
            String act = this.list.get(0);
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void getOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            String exp= "One";
            String act = this.list.get(0);
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void getZero() {
            String[] array = new String[]{};
            this.list.init(array);
            String act = this.list.get(0);
            this.list.clear();
        }

        //=================================================
        //=================== halfRevers ==================
        //=================================================

        @Test
        public void halfReversMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            String[] exp = new String[]{ "Six", "Seven", "Eight", "Nine", "One", "Two", "Three", "Four", "Five"};
            Object[] act = this.list.halfRevers();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void halfReversMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight"};
            this.list.init(array);
            String[] exp = new String[]{"Five", "Six", "Seven", "Eight", "One", "Two", "Three", "Four"};
            Object[] act = this.list.halfRevers();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void halfReversTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String[] exp = new String[]{"Two", "One"};
            Object[] act = this.list.halfRevers();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void halfReversOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            String[] exp = new String[]{"One"};
            Object[] act = this.list.halfRevers();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void halfReversZero() {
            String[] array = new String[]{};
            this.list.init(array);
            Object[] act = this.list.halfRevers();
            this.list.clear();
        }

        //=================================================
        //=================== Revers ======================
        //=================================================

        @Test
        public void reversMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            String[] exp = new String[]{"Nine", "Eight", "Seven", "Six","Five", "Four", "Three", "Two", "One"};
            Object[] act = this.list.revers();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void reversMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five"};
            this.list.init(array);
            String[] exp = new String[]{"Five", "Four", "Three", "Two", "One"};
            Object[] act = this.list.revers();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void reversTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String[] exp = new String[]{"Two", "One"};
            Object[] act = this.list.revers();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void reversOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            String[] exp = new String[]{"One"};
            Object[] act = this.list.revers();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void reversZero() {
            String[] array = new String[]{};
            this.list.init(array);
            Object[] act = this.list.revers();
            this.list.clear();
        }

        //=================================================
        //=================== Set =========================
        //=================================================

        @Test
        public void setMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two", "new", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.set(2,"new");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void setMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four"};
            this.list.init(array);
            String[] exp = new String[]{"One", "Two", "Three", "new"};
            this.list.set(3,"new");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void setTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String[] exp = new String[]{"new", "Two"};
            this.list.set(0,"new");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }
        @Test
        public void setOne() {
            String[] array = new String[]{"One"};
            this.list.init(array);
            String[] exp = new String[]{"new"};
            this.list.set(0,"new");
            Object[] act = this.list.toArray();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void setZero() {
            String[] array = new String[]{};
            this.list.init(array);
            this.list.set(2,"new");
            Object[] act = this.list.toArray();
            this.list.clear();
        }

        @Test
        public void setPositionOut() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            this.list.set(20,"new");
            Object[] act = this.list.toArray();
            this.list.clear();
        }

        @Test
        public void setPositionOut2() {
            String[] array = new String[]{"One", "Two", "Three", "Four"};
            this.list.init(array);
            this.list.set(4,"new");
            Object[] act = this.list.toArray();
            this.list.clear();
        }

        //=================================================
        //=================== Sort ========================
        //=================================================

        @Test
        public void sortManyMore() {
            String[] array = {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve"};
            this.list.init(array);
            String[] exp = {"Twelve", "Five", "Four", "One", "Nine", "Ten", "Six", "Seven", "Two", "Three", "Eleven", "Eight", null, null, null};
            Object[] act = this.list.sort();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void sortMany() {
            String[] array = {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            String[] exp = {"Nine", "Four", "One", "Six", "Seven", "Two", "Three", "Eight", "Five", null};
            Object[] act = this.list.sort();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void sortMany2() {
            String[] array = {"One", "Two", "Three", "Four"};
            this.list.init(array);
            String[] exp = {"Two", "One", "Three", "Four", null, null, null, null, null, null};
            Object[] act = this.list.sort();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }


        @Test
        public void sortTwo() {
            String[] array = {"One", "Two"};
            this.list.init(array);
            String[] exp = {"Two", "One", null, null, null, null, null, null, null, null};
            Object[] act = this.list.sort();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test
        public void sortOne() {
            String[] array = {"One"};
            this.list.init(array);
            String[] exp = {"One", null, null, null, null, null, null, null, null, null};
            Object[] act = this.list.sort();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void sortZero() {
            String[] array = {};
            this.list.init(array);
            String[] exp = {null, null, null, null, null, null, null, null, null, null};
            Object[] act = this.list.sort();
            this.list.clear();
            Assert.assertArrayEquals(exp, act);
        }

        //=================================================
        //=================== Max ========================
        //=================================================

        @Test
        public void maxMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            String exp = "Two";
            String act = this.list.max();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }
        @Test
        public void maxMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five"};
            this.list.init(array);
            String exp = "Two";
            String act = this.list.max();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void maxTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String exp = "Two";
            String act = this.list.max();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void maxOne() {
            String[] array = new String[]{"One"};;
            this.list.init(array);
            String exp = "One";
            String act = this.list.max();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void maxZero() {
            String[] array = new String[]{};
            this.list.init(array);
            String act = this.list.max();
            this.list.clear();
        }

        //=================================================
        //=================== Min ========================
        //=================================================

        @Test
        public void minMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            String exp = "Five";
            String act = this.list.min();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }
        @Test
        public void mbnMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five"};
            this.list.init(array);
            String exp = "Five";
            String act = this.list.min();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void minTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            String exp = "One";
            String act = this.list.min();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void minOne() {
            String[] array = new String[]{"One"};;
            this.list.init(array);
            String exp = "One";
            String act = this.list.min();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void minZero() {
            String[] array = new String[]{};
            this.list.init(array);
            String act = this.list.min();
            this.list.clear();
        }

        //=================================================
        //=================== MaxPos ========================
        //=================================================

        @Test
        public void maxPosMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            int exp = 1;
            int act = this.list.maxPos();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }
        @Test
        public void maxPosMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five"};
            this.list.init(array);
            int exp = 1;
            int act = this.list.maxPos();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void maxPosTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            int exp = 1;
            int act = this.list.maxPos();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void maxPosOne() {
            String[] array = new String[]{"One"};;
            this.list.init(array);
            int exp = 0;
            int act = this.list.maxPos();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void maxPosZero() {
            String[] array = new String[]{};
            this.list.init(array);
            int act = this.list.maxPos();
            this.list.clear();
        }

        //=================================================
        //=================== MinPos ========================
        //=================================================

        @Test
        public void minPosMany() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
            this.list.init(array);
            int exp = 4;
            int act = this.list.minPos();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }
        @Test
        public void minPosMany2() {
            String[] array = new String[]{"One", "Two", "Three", "Four", "Five"};
            this.list.init(array);
            int exp = 4;
            int act = this.list.minPos();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void minPosTwo() {
            String[] array = new String[]{"One", "Two"};
            this.list.init(array);
            int exp = 0;
            int act = this.list.minPos();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test
        public void minPosOne() {
            String[] array = new String[]{"One"};;
            this.list.init(array);
            int exp = 0;
            int act = this.list.minPos();
            this.list.clear();
            Assert.assertEquals(exp, act);
        }

        @Test (expected = ListEmptyException.class)
        public void minPosZero() {
            String[] array = new String[]{};
            this.list.init(array);
            int act = this.list.minPos();
            this.list.clear();
        }
    }
