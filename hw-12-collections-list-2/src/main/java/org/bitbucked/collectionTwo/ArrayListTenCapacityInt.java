package main.java.org.bitbucked.collectionTwo;


import org.bitbucked.collectionOne.IList;
import org.bitbucked.collectionOne.exception.ListEmptyException;

import java.util.ArrayList;

public class ArrayListTenCapacityInt implements IList {

    private int[] array = new int[10];

    private int index;

    private int capacity;

    @Override
    public void init(int[] init) {
        if (init == null) {
            this.index = 0;
        } else {
            this.index = init.length;
            for (int i = 0; i < this.index; i++) {
                this.array[i] = init[i];

            }
        }
    }

    @Override
    public void clear() {
        this.index = 0;
    }

    @Override
    public int size() {
        return this.index;
    }

    @Override
    public int[] toArray() {
        int[] result = new int[this.index];
        for (int i = 0; i < this.index; i++) {
            result[i] = this.array[i];
        }
        return result;
    }

    private void resize() {
        this.capacity = this.capacity * 3 / 2 + 1;
        int[] tmp = new int[this.capacity];
        for (int i = 0; i < this.index; i++) {
            tmp[i] = this.array[i];
        }
        this.array = tmp;
    }


    @Override
    public void addStart(int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        resize();
        for (int i = this.index; i > 0 ; i--) {
            this.array[i] = this.array[i - 1];
        }
        this.array[0] = val;
        this.index++;
    }
    @Override
    public void addEnd(int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        this.array[this.index] = val;
        this.index++;

    }

    @Override
    public void addByPos(int pos, int val) {
        int size = size();
        if (index > size || index < 0) {
            throw new ListEmptyException();
        }
        while (this.size() + 1 > this.index * 7 / 10) {
            resize();
        }
        int[] tmp = new int[this.index];
        for (int i = this.index; i > 0; i--) {
            if (i > pos)
                tmp[i] = this.array[i];
            else if (i < pos)
                tmp[i] = this.array[i + 1];
        }
        this.array[0] = val;
        this.index++;
    }

    @Override
    public int removeStart() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        for (int i = this.index; i < 0; i--) {
            this.array[i-1] = this.array[i];
        }
        this.index--;
        return this.index;
    }


    @Override
    public int removeEnd() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        for (int i = this.index; i < 0; i--) {
            this.array[i] = this.array[i];
        }
        this.index--;
        return this.index;
    }

    @Override
    public int removeByPos(int pos) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        this.index--;
        int[] tmp = new int[this.index];
        int remove = this.array[pos];
        for (int i = 0; i < this.index; i++) {
            if(pos <= i){
                tmp[i] = this.array[i+1];
            } else {
                tmp[i] = this.array[i];
            }
        }
        this.array = tmp;
        return remove;
    }

    @Override
    public int max() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int max = this.array[0];
        for (int i = 1; i < this.index; i++){
            if (this.array[i] > max){
                max = this.array[i];
            }
        }
        return max;
    }

    @Override
    public int min() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int min = this.array[0];
        for (int i = 1; i < this.index; i++){
            if (this.array[i] < min){
                min = this.array[i];
            }
        }
        return min;
    }

    @Override
    public int maxPos() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int max = this.array[0];
        int index = 0;
        for (int i = 1; i < this.index; i++){
            if (this.array[i] > max){
                max = this.array[i];
                index = i;
            }
        }
        return index;
    }

    @Override
    public int minPos() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int min = this.array[0];
        int index = 0;
        for (int i = 1; i < this.index; i++){
            if (this.array[i] < min){
                min = this.array[i];
                index = i;
            }
        }
        return index;
    }

    @Override
    public int[] sort() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        for(int i = 0; i < this.index; i++){
            for(int j = 0; j < this.index-i-1; j++){
                if(this.array[j] > this.array[j+1]){
                    int temp = this.array[j];
                    this.array[j] = this.array[j+1];
                    this.array[j+1] = temp;
                }
            }
        }
        return this.array;
    }

    @Override
    public int get(int pos) {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        return this.array[pos];
    }

    @Override
    public int[] halfRevers() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int[] result = new int[this.index];
        int half;
        if(this.index%2 == 0){
            half = this.index/2;
        } else {
            half = (this.index+1)/2;
        }
        for (int i = half; i < this.index; i++){
            result[i-half] = this.array[i];
        }
        for (int j = 0; j < half; j++){
            result [j+this.index-half] = this.array[j];
        }
        return result;
    }

    @Override
    public int[] revers() {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int[] result = new int[this.index];
        for (int i = 0; i < this.index; i++){
            result[i] = this.array[this.index-1-i];
        }
        return result;
    }

    @Override
    public void set(int pos, int val) {
        if (this.index == 0) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[this.index];
        for (int i = 0; i < this.index; i++) {
            if (pos == i){
                tmp[i] = val;
            } else {
                tmp[i] = this.array[i];
            }
        }
        this.array = tmp;
    }
}
