package com.guthub.var2;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

    public Frame() throws HeadlessException {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame("A World of Balls");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setContentPane(new BallWorld(800, 700)); // BallWorld is a JPanel
                frame.pack();            // Предпочтительный размер BallWorld
                frame.setVisible(true);  // Show it
            }
        });
    }
}