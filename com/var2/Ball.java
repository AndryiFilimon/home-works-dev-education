package com.guthub.var2;

import com.guthub.var2.lb.CollisionPhysics;
import com.guthub.var2.physics.CollisionResponse;

import javax.swing.*;
import java.awt.*;
import java.util.Formatter;
import java.util.Random;

public class Ball extends JPanel {
    float x, y;           // Центр мяча по осям x и y (доступ к пакету)
    float speedX, speedY; // Скорость мяча на шаг по x и y (доступ к пакету)
    float radius;  // Ball's radius (package access)
    Point point;
    private Color color;  // Ball's color

    Random random = new Random();
    int r = random.nextInt(255);
    int g = random.nextInt(255);
    int b = random.nextInt(255);
    Color newColor = new Color(r, g, b);

    // Для обнаружения столкновений и ответа
    // Сохраняем реакцию на самое раннее обнаруженное столкновение
    // этим экземпляром шара. Имеет значение только первое столкновение! (доступ к пакету)

    CollisionResponse earliestCollisionResponse = new CollisionResponse();

    /**
     * Конструктор: для удобства пользователя пользователь указывает скорость в скорости и
     * moveAngle в обычных декартовых координатах. Необходимо преобразовать в speedX и
     * speedY в графических координатах Java для простоты работы.
     */
    public Ball(float x, float y, float radius, float speed, float angleInDegree,
                Color color) {
        this.x = x;
        this.y = y;
        // Преобразование (скорость, угол) в (x, y) с перевернутой осью y
        this.speedX = (float) (speed * Math.cos(Math.toRadians(angleInDegree)));
        this.speedY = (float) (-speed * (float) Math.sin(Math.toRadians(angleInDegree)));
        this.radius = random.nextInt(Math.max(30,120));
        this.color = newColor;

    }


    // Рабочая копия для вычисления ответа в crossct (),
    // чтобы избежать повторного выделения объектов.
    private CollisionResponse tempResponse = new CollisionResponse();

    public Ball(Point point) {
    }


    /**
     * Проверьте, не сталкивается ли этот шар с контейнером на следующем временном шаге.
     *
     * @param box: внешний прямоугольный контейнер.
     * @param timeLimit: верхняя граница временного интервала.
     */
    public void intersect(ContainerBox box, float timeLimit) {
        // Вызов movingPointIntersectsRectangleOuter, который возвращает
        // самое раннее столкновение с одной из 4 границ, если столкновение обнаружено.
        CollisionPhysics.pointIntersectsRectangleOuter(x, y, speedX, speedY, radius,
                box.minX, box.minY, box.maxX, box.maxY, timeLimit, tempResponse);
        if (tempResponse.t < earliestCollisionResponse.t) {
            earliestCollisionResponse.copy(tempResponse);
        }
    }

    // Рабочая копия для вычисления ответа в пересечении (Ball, timeLimit),
    // чтобы избежать повторного выделения объектов.
    private CollisionResponse thisResponse = new CollisionResponse();
    private CollisionResponse anotherResponse = new CollisionResponse();

    /**
     * Проверить, сталкивается ли этот шар с заданным другим шаром в интервале
     * (0, предел времени].
     *
     * @param another:   проверяется на столкновение еще один движущийся мяч.
     * @param timeLimit: верхняя граница временного интервала.
     */
    public void intersect(Ball another, float timeLimit) {
        // Вызов функции movingPointIntersectsMovingPoint () с timeLimit.
        // Используйте thisResponse и anotherResponse в качестве рабочих копий для хранения
        // ответы этого шара и другого шара соответственно.
        // Проверяем, является ли это столкновение самым ранним, и обновляем мяч
        // EarlyCollisionResponse соответственно.
        CollisionPhysics.pointIntersectsMovingPoint(
                this.x, this.y, this.speedX, this.speedY, this.radius,
                another.x, another.y, another.speedX, another.speedY, another.radius,
                timeLimit, thisResponse, anotherResponse);

        if (anotherResponse.t < another.earliestCollisionResponse.t) {
            another.earliestCollisionResponse.copy(anotherResponse);
        }
        if (thisResponse.t < this.earliestCollisionResponse.t) {
            this.earliestCollisionResponse.copy(thisResponse);
        }
    }

    /**
     * Обновите состояния этого шара за заданное время.
     *
     * @param time: самое раннее время столкновения, обнаруженное в системе.
     *              Если самое раннееCollisionResponse.time этого мяча равно времени, это
     *              мяч столкнулся; в противном случае произойдет столкновение в другом месте.
     */
    public void update(float time) {
        // Проверить, отвечает ли этот шар за первое столкновение?
        if (earliestCollisionResponse.t <= time) {
            // Этот мяч столкнулся, получить новую позицию и скорость
            this.x = earliestCollisionResponse.getNewX(this.x, this.speedX);
            this.y = earliestCollisionResponse.getNewY(this.y, this.speedY);
            this.speedX = earliestCollisionResponse.newSpeedX;
            this.speedY = earliestCollisionResponse.newSpeedY;
        } else {
            //Этот мяч не участвует в столкновении. Двигайтесь прямо.
            this.x += this.speedX * time;
            this.y += this.speedY * time;
        }
        // Очистить для следующего обнаружения столкновения
        earliestCollisionResponse.reset();
    }

    /* Нарисуйте себя, используя данный графический контекст.*/
    public void draw(Graphics g) {
        g.setColor(color);
        g.fillOval((int) (x - radius), (int) (y - radius), (int) (2 * radius),
                (int) (2 * radius));
    }

    /* Вернуть величину скорости. */
    public float getSpeed() {
        return (float) Math.sqrt(speedX * speedX + speedY * speedY);
    }

    /* Верните направление движения в градусах (против часовой стрелки) */
    public float getMoveAngle() {
        return (float) Math.toDegrees(Math.atan2(-speedY, speedX));
    }

    /* Возвратная масса */
    public float getMass() {
        return radius * radius * radius / 1000f;
    }

    /* Вернуть кинетическую энергию (0,5 мВ ^ 2)*/
    public float getKineticEnergy() {
        return 0.5f * getMass() * (speedX * speedX + speedY * speedY);
    }

    /*Опишите себя. */
    public String toString() {
        sb.delete(0, sb.length());
        formatter.format("@(%3.0f,%3.0f) r=%3.0f V=(%3.0f,%3.0f) " +
                        "S=%4.1f \u0398=%4.0f KE=%3.0f",
                x, y, radius, speedX, speedY, getSpeed(), getMoveAngle(),
                getKineticEnergy());  // \u0398 is theta
        return sb.toString();
    }

    private StringBuilder sb = new StringBuilder();
    private Formatter formatter = new Formatter(sb);

}
