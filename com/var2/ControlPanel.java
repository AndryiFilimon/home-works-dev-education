package com.guthub.var2;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ControlPanel extends JPanel {

    private static final int MAX_BALLS = 25;
    private boolean paused = false;
    private Point point;
    private Ball[] balls = new Ball[MAX_BALLS];
    private int currentNumBalls;

    /**
     * Constructor to initialize UI components of the controls
     */
//    public ControlPanel(Point point) {
//        this.point = point;
//    }

    public ControlPanel(Point point) {
        this.point = point;
        // A checkbox to toggle pause/resume movement
        JCheckBox pauseControl = new JCheckBox();
        this.add(new JLabel("Pause"));
        this.add(pauseControl);
        pauseControl.addItemListener(e -> {
            paused = !paused;  // Toggle pause/resume flag
            transferFocusUpCycle();  // To handle key events
        });

        // A slider for adjusting the speed of all the balls by a factor
        final float[] ballSavedSpeedXs = new float[MAX_BALLS];
        final float[] ballSavedSpeedYs = new float[MAX_BALLS];
        for (int i = 0; i < currentNumBalls; i++) {
            ballSavedSpeedXs[i] = balls[i].speedX;
            ballSavedSpeedYs[i] = balls[i].speedY;
        }
        int minFactor = 5;    // percent
        int maxFactor = 200;  // percent
        JSlider speedControl = new JSlider(JSlider.HORIZONTAL, minFactor, maxFactor, 100);
        this.add(new JLabel("Speed"));
        this.add(speedControl);
        speedControl.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                if (!source.getValueIsAdjusting()) {
                    int percentage = (int) source.getValue();
                    for (int i = 0; i < currentNumBalls; i++) {
                        balls[i].speedX = ballSavedSpeedXs[i] * percentage / 100.0f;
                        balls[i].speedY = ballSavedSpeedYs[i] * percentage / 100.0f;
                    }
                }
                transferFocusUpCycle();  // To handle key events
            }
        });

        // A button for launching the remaining balls
        final JButton launchControl = new JButton("Launch New Ball");
        this.add(launchControl);
        launchControl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentNumBalls < MAX_BALLS) {
                    balls[currentNumBalls].x = 20;
                    balls[currentNumBalls].y = 20;
                    currentNumBalls++;
                    if (currentNumBalls == MAX_BALLS) {
                        // Disable the button, as there is no more ball
                        launchControl.setEnabled(false);
                    }
                }
                transferFocusUpCycle();  // To handle key events
            }
        });
    }
}