package com.guthub.var2.physics;

public class Physics {

    private static CollisionResponse tempResponse = new CollisionResponse();

    public static void pointIntersectsRectangleOuter(
            float pointX, float pointY, float speedX, float speedY, float radius,
            float rectX1, float rectY1, float rectX2, float rectY2,
            float timeLimit, CollisionResponse response) {


// Предположения:
        assert (rectX1 < rectX2) && (rectY1 < rectY2) : "Malformed rectangle!";
        assert (pointX >= rectX1 + radius) && (pointX <= rectX2 - radius)
                && (pointY >= rectY1 + radius) && (pointY <= rectY2 - radius)
                : "Point (with radius) is outside the rectangular container!";
        assert (radius >= 0) : "Negative radius!";
        assert (timeLimit > 0) : "Non-positive time";

        response.reset();  // Сбрасываем время обнаруженного столкновения на бесконечность

        // Внешний прямоугольный контейнерный бокс имеет 4 границы.
        // Нужно искать самое раннее столкновение, если оно есть.

        // Правая граница
        pointIntersectsLineVertical(pointX, pointY, speedX, speedY, radius,
                rectX2, timeLimit, tempResponse);
        if (tempResponse.t < response.t) {
            response.copy(tempResponse);// Копируем в результирующий ответ
        }
        // Левая граница
        pointIntersectsLineVertical(pointX, pointY, speedX, speedY, radius,
                rectX1, timeLimit, tempResponse);
        if (tempResponse.t < response.t) {
            response.copy(tempResponse);// Копируем в результирующий ответ
        }
        // Верхняя граница
        pointIntersectsLineHorizontal(pointX, pointY, speedX, speedY, radius,
                rectY1, timeLimit, tempResponse);
        if (tempResponse.t < response.t) {
            response.copy(tempResponse);  // Copy into resultant response
        }
        // Нижняя граница
        pointIntersectsLineHorizontal(pointX, pointY, speedX, speedY, radius,
                rectY2, timeLimit, tempResponse);
        if (tempResponse.t < response.t) {
            response.copy(tempResponse);  // Copy into resultant response
        }

        // FIXME: Что если два столкновения одновременно ??
        // Объект CollisionResponse сохраняет результат одного столкновения, а не обоих!
    }

    public static void pointIntersectsMovingPoint(
            float p1X, float p1Y, float p1SpeedX, float p1SpeedY, float p1Radius,
            float p2X, float p2Y, float p2SpeedX, float p2SpeedY, float p2Radius,
            float timeLimit, CollisionResponse p1Response, CollisionResponse p2Response) {

        // Предположения:
        assert (p1Radius >= 0) && (p2Radius >= 0) : "Negative radius!";
        assert timeLimit > 0 : "Non-positive time!";

        p1Response.reset();  // Устанавливаем время обнаруженного столкновения на бесконечность
        p2Response.reset();

        // Вызов вспомогательного метода для вычисления времени столкновения t.
        float t = pointIntersectsMovingPointDetection(
                p1X, p1Y, p1SpeedX, p1SpeedY, p1Radius,
                p2X, p2Y, p2SpeedX, p2SpeedY, p2Radius);

        // Принимаем 0 <t <= timeLimit
        if (t > 0 && t <= timeLimit) {
            // Вызов вспомогательного метода для вычисления ответов в 2 объектах Response
            pointIntersectsMovingPointResponse(
                    p1X, p1Y, p1SpeedX, p1SpeedY, p1Radius,
                    p2X, p2Y, p2SpeedX, p2SpeedY, p2Radius,
                    p1Response, p2Response, t);
        }
    }

    public static void pointIntersectsLineHorizontal(
            float pointX, float pointY, float speedX, float speedY, float radius,
            float lineY, float timeLimit, CollisionResponse response) {

        // Assumptions:
        assert (radius >= 0) : "Negative radius!";
        assert (timeLimit > 0) : "Non-positive time";

        response.reset();
        // Сбрасываем время обнаруженного столкновения на бесконечность


        // Коллизия невозможна, если скорость Y равна нулю
        if (speedY == 0) { // Следует ли использовать порог?
            return;
        }

        // Вычислить расстояние до линии со смещением по радиусу.
        float distance;
        if (lineY > pointY) {
            distance = lineY - pointY - radius;
        } else {
            distance = lineY - pointY + radius;
        }

        float t = distance / speedY;  // speedY != 0
        // Принимаем 0 <t <= timeLimit
        if (t > 0 && t <= timeLimit) {
            response.t = t;
            response.newSpeedY = -speedY; // Отражаем вертикально
            response.newSpeedX = speedX;  // Без изменений по горизонтали
        }
    }

    public static void pointIntersectsLineVertical(
            float pointX, float pointY, float speedX, float speedY, float radius,
            float lineX, float timeLimit, CollisionResponse response) {

        // Assumptions:
        assert (radius >= 0) : "Negative radius!";
        assert (timeLimit > 0) : "Non-positive time";

        response.reset();  // Reset detected collision time to infinity

        // Коллизия невозможна, если speedX равен нулю
        if (speedX == 0) { // FIXME: Should I use a threshold?
            return;
        }

        // Вычислить расстояние до линии со смещением по радиусу.
        float distance;
        if (lineX > pointX) {
            distance = lineX - pointX - radius;
        } else {
            distance = lineX - pointX + radius;
        }

        float t = distance / speedX;  // speedX != 0
        // Принимаем 0 < t <= timeLimit
        if (t > 0 && t <= timeLimit) {
            response.t = t;
            response.newSpeedX = -speedX;  // Отражаем horizontally
            response.newSpeedY = speedY;   // Без изменений по vertically
        }
    }

    public static float pointIntersectsMovingPointDetection(
            float p1X, float p1Y, float p1SpeedX, float p1SpeedY, float p1Radius,
            float p2X, float p2Y, float p2SpeedX, float p2SpeedY, float p2Radius) {

        // Переставляем параметры, чтобы получить квадратное уравнение.
        double centerX = p1X - p2X;
        double centerY = p1Y - p2Y;
        double speedX = p1SpeedX - p2SpeedX;
        double speedY = p1SpeedY - p2SpeedY;
        double radius = p1Radius + p2Radius;
        double radiusSq = radius * radius;
        double speedXSq = speedX * speedX;
        double speedYSq = speedY * speedY;
        double speedSq = speedXSq + speedYSq;

        //Решите квадратное уравнение для времени столкновения t
        double termB2minus4ac = radiusSq * speedSq - (centerX * speedY - centerY * speedX)
                * (centerX * speedY - centerY * speedX);
        if (termB2minus4ac < 0) {
            //Нет пересечения.
            //Движущиеся сферы могут пересекаться в разное время или двигаться параллельно.
            return Float.MAX_VALUE;
        }

        double termMinusB = -speedX * centerX - speedY * centerY;
        double term2a = speedSq;
        double rootB2minus4ac = Math.sqrt(termB2minus4ac);
        double sol1 = (termMinusB + rootB2minus4ac) / term2a;
        double sol2 = (termMinusB - rootB2minus4ac) / term2a;
        // Принимаем наименьшее положительное значение t в качестве решения.
        if (sol1 > 0 && sol2 > 0) {
            return (float) Math.min(sol1, sol2);
        } else if (sol1 > 0) {
            return (float) sol1;
        } else if (sol2 > 0) {
            return (float) sol2;
        } else {
            // Нет положительного t решения. Установите время обнаружения столкновения на бесконечность.
            return Float.MAX_VALUE;
        }
    }

    public static void pointIntersectsMovingPointResponse(
            float p1X, float p1Y, float p1SpeedX, float p1SpeedY, float p1Radius,
            float p2X, float p2Y, float p2SpeedX, float p2SpeedY, float p2Radius,
            CollisionResponse p1Response, CollisionResponse p2Response, float t) {

        //Обновите обнаруженное время столкновения в CollisionResponse.
        p1Response.t = t;
        p2Response.t = t;

        //Получите точку удара, чтобы сформировать линию столкновения.
        double p1ImpactX = p1Response.getImpactX(p1X, p1SpeedX);
        double p1ImpactY = p1Response.getImpactY(p1Y, p1SpeedY);
        double p2ImpactX = p2Response.getImpactX(p2X, p2SpeedX);
        double p2ImpactY = p2Response.getImpactY(p2Y, p2SpeedY);

        // Направление вдоль линии столкновения - P, нормальное - N.
        // Получаем направление по линии столкновения
        double lineAngle = Math.atan2(p2ImpactY - p1ImpactY, p2ImpactX - p1ImpactX);

        //Скорости проекта от (x, y) до (p, n)
        double[] result = rotate(p1SpeedX, p1SpeedY, lineAngle);
        double p1SpeedP = result[0];
        double p1SpeedN = result[1];
        result = rotate(p2SpeedX, p2SpeedY, lineAngle);
        double p2SpeedP = result[0];
        double p2SpeedN = result[1];

        // Коллизия возможна только если p1SpeedP - p2SpeedP> 0
        // Требуется, если два шара перекрываются в исходном положении
        // Не объявлять столкновение, чтобы они продолжили движение
        // пока они не разделятся.
        if (p1SpeedP - p2SpeedP <= 0) {
            //System.out.println ("скорости не могут столкнуться! T =" + t);
            p1Response.reset();  // Установить время столкновения на бесконечность
            p2Response.reset();
            return;
        }

        // Предположим, что масса пропорциональна кубу радиуса.
        // (Все объекты имеют одинаковую плотность.)
        double p1Mass = p1Radius * p1Radius * p1Radius;
        double p2Mass = p2Radius * p2Radius * p2Radius;
        double diffMass = p1Mass - p2Mass;
        double sumMass = p1Mass + p2Mass;

        double p1SpeedPAfter, p1SpeedNAfter, p2SpeedPAfter, p2SpeedNAfter;
        // Вдоль направления столкновения P примените закон сохранения энергии и импульса.
        p1SpeedPAfter = (diffMass * p1SpeedP + 2.0 * p2Mass * p2SpeedP) / sumMass;
        p2SpeedPAfter = (2.0 * p1Mass * p1SpeedP - diffMass * p2SpeedP) / sumMass;

        //Без изменений в перпендикулярном направлении N
        p1SpeedNAfter = p1SpeedN;
        p2SpeedNAfter = p2SpeedN;

        //Спроецируйте скорости обратно от (p, n) к (x, y)
        result = rotate(p1SpeedPAfter, p1SpeedNAfter, -lineAngle);
        p1Response.newSpeedX = (float) result[0];
        p1Response.newSpeedY = (float) result[1];
        result = rotate(p2SpeedPAfter, p2SpeedNAfter, -lineAngle);
        p2Response.newSpeedX = (float) result[0];
        p2Response.newSpeedY = (float) result[1];
    }

    private static double[] rotateResult = new double[2];

    private static double[] rotate(double x, double y, double theta) {
        double sinTheta = Math.sin(theta);
        double cosTheta = Math.cos(theta);
        rotateResult[0] = x * cosTheta + y * sinTheta;
        rotateResult[1] = -x * sinTheta + y * cosTheta;
        return rotateResult;
    }

}