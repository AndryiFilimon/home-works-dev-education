package com.guthub.var2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Random;

public class BallWorld extends JPanel {

    private static final int UPDATE_RATE = 30;    // Кадров в секунду (fps)
    private static final float EPSILON_TIME = 1e-2f;  // Порог для нулевого времени


    // Balls
    private static final int MAX_BALLS = 25; // Максимальное допустимое количество
    private int currentNumBalls;             // Номер в настоящее время активен
    private Ball[] balls = new Ball[MAX_BALLS];

    private ContainerBox box;     // Контейнер прямоугольный ящик
    private DrawCanvas canvas;    // Пользовательский холст для рисования коробки / мяча
    private int canvasWidth;
    private int canvasHeight;

    private PanelBalls control; // Панель управления кнопками и ползунками.
    private boolean paused = false;  // Флаг для управления паузой / возобновлением

    Random random = new Random();
    float newRadius = random.nextInt(Math.max(25, 90));
    int newX = random.nextInt(Math.max(1,690));
    int newY = random.nextInt(Math.max(1,690));

    float newSpeed = random.nextInt(Math.max(1,15));


    /**
     * Конструктор для создания компонентов пользовательского интерфейса и инициализации игровых объектов.
     * Установите холст для рисования на весь экран (с учетом его ширины и высоты).
     *
     * @param width:  ширина экрана
     * @param height: высота экрана
     */
    public BallWorld(int width, int height) {
        final int controlHeight = 30;
        canvasWidth = width;
        canvasHeight = height - controlHeight;  //Оставьте место для панели управления


        currentNumBalls = 4;
        balls[0] = new Ball(newX, newY, newRadius, newSpeed, 34, Color.getHSBColor(255,255,255));
        balls[1] = new Ball(newX, newY, newRadius, 2, -114, Color.getHSBColor(255,255,255));
        balls[2] = new Ball(newX, newY, newRadius, 3, 14, Color.getHSBColor(255,255,255));
        balls[3] = new Ball(newX, newY, newRadius, 3, 14, Color.getHSBColor(255,255,255));

        //Остальные шары, которые можно запустить с помощью кнопки запуска
        for (int i = currentNumBalls; i < MAX_BALLS; i++) {
            // Распределите шары, но позже перед запуском
            balls[i] = new Ball(20, canvasHeight - 20, newRadius, 5, 45, Color.getHSBColor(255,255,255));
        }

        // Запустите контейнерную коробку, чтобы заполнить экран
        box = new ContainerBox(0, 0, canvasWidth, canvasHeight, Color.BLACK, Color.WHITE);

        // Запустите настраиваемую панель рисования для рисования игры
        canvas = new DrawCanvas();

        // Запустить панель управления
        control = new PanelBalls();
        control.setBounds(0, 0, 750, 650);
        control.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f)));

        //Макет панели рисования и панели управления
        this.setLayout(new BorderLayout());
        this.add(canvas, BorderLayout.CENTER);
        this.add(control, BorderLayout.SOUTH);


        // Обработка изменения размера окна. Отрегулируйте контейнер для заполнения экрана.
        this.addComponentListener(new ComponentAdapter() {
            // Called back for first display and subsequent window resize.
            @Override
            public void componentResized(ComponentEvent e) {
                Component c = (Component) e.getSource();
                Dimension dim = c.getSize();
                canvasWidth = dim.width;
                canvasHeight = dim.height - controlHeight; // Оставьте место для панели управления
                // Необходимо изменить размер всех компонентов, чувствительных к размеру экрана.
                box.set(0, 0, canvasWidth, canvasHeight);
            }
        });

        // Начать подпрыгивание мяча
        gameStart();
    }

    /**
     * Start the ball bouncing.
     */
    public void gameStart() {
        // Запустите игровую логику в отдельном потоке.
        Thread gameThread = new Thread(() -> {
            while (true) {
                long beginTimeMillis, timeTakenMillis, timeLeftMillis;
                beginTimeMillis = System.currentTimeMillis();

                if (!paused) {
                    // Выполнить один шаг игры
                    gameUpdate();
                    // Обновите дисплей
                    repaint();
                }

                // Обеспечьте необходимую задержку для достижения целевой ставки
                timeTakenMillis = System.currentTimeMillis() - beginTimeMillis;
                timeLeftMillis = 1000L / UPDATE_RATE - timeTakenMillis;
                if (timeLeftMillis < 5) timeLeftMillis = 5; // Set a minimum

                // Отложите и дайте шанс другому потоку
                try {
                    Thread.sleep(timeLeftMillis);
                } catch (InterruptedException ex) {
                }
            }
        });
        gameThread.start();  // Invoke GaemThread.run()
    }

    /*  Один игровой временной шаг.
     * Обновите игровые объекты с правильным обнаружением столкновений и реагированием.
     */
    public void gameUpdate() {
        float timeLeft = 1.0f;  // Один временной шаг для начала

        // Повторяйте, пока не истечет один временной шаг.
        do {
            // Найдите самое раннее столкновение до timeLeft среди всех объектов
            float tMin = timeLeft;

            // Проверить столкновение между двумя шарами
            for (int i = 0; i < currentNumBalls; i++) {
                for (int j = 0; j < currentNumBalls; j++) {
                    if (i < j) {
                        balls[i].intersect(balls[j], tMin);
                        if (balls[i].earliestCollisionResponse.t < tMin) {
                            tMin = balls[i].earliestCollisionResponse.t;
                        }
                    }
                }
            }
            // Проверить столкновение между шарами и коробкой
            for (int i = 0; i < currentNumBalls; i++) {
                balls[i].intersect(box, tMin);
                if (balls[i].earliestCollisionResponse.t < tMin) {
                    tMin = balls[i].earliestCollisionResponse.t;
                }
            }

            // Обновляем все шары до обнаруженного самого раннего времени столкновения tMin,
            // или timeLeft, если коллизии нет.
            for (int i = 0; i < currentNumBalls; i++) {
                balls[i].update(tMin);
            }

            timeLeft -= tMin;                // Вычтите затраченное время и повторите
        } while (timeLeft > EPSILON_TIME);  // Игнорировать оставшееся время меньше порогового
    }

    /* Пользовательская панель рисования для прыгающего мяча (внутренний класс).*/
    class DrawCanvas extends JPanel {
        /**
         * Пользовательские коды чертежей
         */
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);    // Краска фон
            // Нарисуйте шары и коробку
            box.draw(g);
            for (int i = 0; i < currentNumBalls; i++) {
                balls[i].draw(g);
            }
            // Отображение информации о шарах
            g.setColor(Color.WHITE);
            g.setFont(new Font("Courier New", Font.PLAIN, 12));
            for (int i = 0; i < currentNumBalls; i++) {
                g.drawString("Ball " + (i + 1) + " " + balls[i].toString(), 20, 30 + i * 20);
            }
        }

        /**
         * Вызывается, чтобы получить предпочтительный размер компонента.
         */
        @Override
        public Dimension getPreferredSize() {
            return (new Dimension(canvasWidth, canvasHeight));
        }
    }

    /** Панель управления (внутренний класс). */

    /**
     * The control panel (inner class).
     */
//     class ControlPanel extends JPanel {


        }

