package com.company;

public class Arrays {
    public static void main(String[] args) {

        String[][] m = {{"*","*","*","*","*"},{" ","*"," ","*"," "}
        ,{" "," ","*"," "," "}};

        for (int row = 0; row < m.length; row++) {
            for (int col = 0; col <m[row].length; col++) {
                System.out.print(m[row][col] + "  ");
            }
            System.out.println();
        }
    }
}
