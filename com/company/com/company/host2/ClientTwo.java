package com.company.host2;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ClientTwo {

    public static void main(String[] args) throws IOException {

        Socket socket = new Socket();
        socket.connect(new InetSocketAddress("localhost", 8080), 2000);
        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

        String line;
        while ((line = br.readLine()) != null && !line.isEmpty()){
            System.out.println(line);
        }



    }
}
