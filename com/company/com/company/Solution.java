package com.company;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.print("Введите число - ");
        int x = scan.nextInt();
        int result = SoluTion(x);

    }

    public static int SoluTion(int a) {
        int num = 1;
        int res = 0;
        while(a > 0){
            a = a - num;
            num = num + 2;
            res += (a < 0) ? 0:1;

        }
        return res;
    }
}
