package com.github.drawingstarsfigures.DrawingStarsSix;

public class DrawingStarsSix {
    public static void main(String[] args) {
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == 0 || j == 6 || i == j)
                    System.out.print("*\t");
                else
                    System.out.print("\t");
            }
            System.out.println();
        }
    }
}
