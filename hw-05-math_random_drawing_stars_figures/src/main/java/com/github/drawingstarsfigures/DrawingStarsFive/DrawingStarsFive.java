package com.github.drawingstarsfigures.DrawingStarsFive;

public class DrawingStarsFive {
    public static void main(String[] args) {
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == 6 || j == 6 || i == (array[1].length - j - 1))
                    System.out.print("*\t");
                else
                    System.out.print("\t");
            }
            System.out.println();
        }
    }
}
