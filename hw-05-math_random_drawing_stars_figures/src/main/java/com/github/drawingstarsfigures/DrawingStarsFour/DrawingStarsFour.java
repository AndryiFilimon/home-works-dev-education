package com.github.drawingstarsfigures.DrawingStarsFour;

public class DrawingStarsFour {
    public static void main(String[] args) {
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i == 6 || j == 0 || i == j)
                    System.out.print("*\t");
                else
                    System.out.print("\t");
            }
            System.out.println();
        }
    }
}
