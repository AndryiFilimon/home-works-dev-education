package com.github.drawingstarsfigures.DrawingStarsEight;

public class DrawingStarsEight {
    public static void main(String[] args) {
        String[][] array = new String[7][7];
        for (int i = 0; i < array[0].length; i++) {
            for (int j = 0; j < array[1].length; j++) {
                if (i > array[0].length / 2) {
                    break;
                }
                if (i == 0 || i == (array[1].length - j - 1) || i == j) {
                    System.out.print("*\t");
                } else {
                    System.out.print(" \t");
                }
            }
            System.out.println();
        }
    }
}
