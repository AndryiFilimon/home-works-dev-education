package com.github.math.SpeedAvto;

import java.util.Scanner;

public class SpeedAvto {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите скорость первого авто(км/ч): ");
        int v1 = scan.nextInt();
        System.out.println("Введите скорость второго авто(км/ч): ");
        int v2 = scan.nextInt();
        System.out.println("Введите расстояние авто(км): ");
        int s = scan.nextInt();
        System.out.println("Введите время(ч): ");
        int t = scan.nextInt();
        int result = speedAvto(s, t, v1, v2);
        System.out.println(result);
    }


    public static int speedAvto(int s, int t, int v1, int v2) {
        return s + (t * (v1 + v2));
    }
}
