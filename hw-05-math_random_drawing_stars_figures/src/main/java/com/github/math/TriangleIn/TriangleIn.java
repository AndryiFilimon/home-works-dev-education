package com.github.math.TriangleIn;

public class TriangleIn {
    public static void main(String[] args) {
        int y = 3;
        int x = -1;
        boolean free = triangleIn(x, y);
        System.out.println("You result:" + free);
    }


    public static boolean triangleIn(int x, int y) {
        boolean result = (x >= 0) && (y >= 1.5 * x-1) &&(y <= x) || (x <= 0) && (y >=-1.5 * x-1) &&(y <=-x);
        return result;
    }
}