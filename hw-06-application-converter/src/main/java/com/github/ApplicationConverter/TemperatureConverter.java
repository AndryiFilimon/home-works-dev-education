package com.github.ApplicationConverter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class TemperatureConverter extends JPanel {
    enum Convert {
        C("Сelsius"),
        K("Kelvin"),
        F("Fahrenheit"),
        Re("Reaumur"),
        Ro("Roemer"),
        Ra("Rankin"),
        N("Newton"),
        D("Delisle");


        private String description;

        Convert(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return this.name() + " - " + this.description;
        }
    }

    class ConvertPair {
        private final Convert from;
        private final Convert to;

        public ConvertPair(Convert from, Convert to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ConvertPair that = (ConvertPair) o;
            if (from != that.from) return false;
            return to == that.to;
        }

        @Override
        public int hashCode() {
            int result = from.hashCode();
            result = 31 * result + to.hashCode();
            return result;
        }
    }
    private final Map<ConvertPair, BigDecimal> exchangeRates = new HashMap<ConvertPair, BigDecimal>() {
        {
            put(new ConvertPair(Convert.C, Convert.C), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.K, Convert.K), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.F, Convert.F), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.Re, Convert.Re), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.Ro, Convert.Ro), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.Ra, Convert.Ra), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.N, Convert.N), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.D, Convert.D), BigDecimal.valueOf(1));

            put(new ConvertPair(Convert.C, Convert.K), BigDecimal.valueOf(273.15));
            put(new ConvertPair(Convert.C, Convert.F), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.C, Convert.Re), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.C, Convert.Ro), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.C, Convert.Ra), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.C, Convert.N), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.C, Convert.D), BigDecimal.valueOf(1));

        }
    };

    public TemperatureConverter() {
        super(new FlowLayout(FlowLayout.LEADING));

        // From
        JPanel from = new JPanel();
        JComboBox fromOptions = new JComboBox(Convert.values());
        from.add(fromOptions);
        from.setBorder(BorderFactory.createTitledBorder("Select Convert"));
        add(from, BorderLayout.CENTER);

        // Amount
        JTextField amountInput = new JTextField(20);
        JPanel amount = new JPanel();
        amount.add(amountInput);
        amount.setBorder(BorderFactory.createTitledBorder("Enter Ammount"));
        add(amount, BorderLayout.CENTER);

        // To
        JComboBox toOptions = new JComboBox(Convert.values());
        JPanel to = new JPanel();
        to.add(toOptions);
        to.setBorder(BorderFactory.createTitledBorder("Convert to"));
        add(to, BorderLayout.CENTER);

        // Convert Action
        JLabel convertText = new JLabel();
        JButton convertCmd = new JButton("Convert");
        convertCmd.addActionListener(convertAction(amountInput, fromOptions, toOptions, convertText));
        JPanel convert = new JPanel();
        convert.add(convertCmd);
        convert.add(convertText);
        add(convert);
    }

    private ActionListener convertAction(
            final JTextField amountInput,
            final JComboBox fromOptions,
            final JComboBox toOptions,
            final JLabel convertText) {

        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String amountInputText = amountInput.getText();
                if ("".equals(amountInputText)) {
                    return;
                }

                // Convert
                BigDecimal conversion = convertConvert(amountInputText);
                convertText.setText(conversion.toString());
            }

            private BigDecimal convertConvert(String amountInputText) {
                ConvertPair ConvertPair = new ConvertPair(
                        (Convert) fromOptions.getSelectedItem(),
                        (Convert) toOptions.getSelectedItem());
                BigDecimal rate = exchangeRates.get(ConvertPair);
                BigDecimal amount = new BigDecimal(amountInputText);
                return amount.multiply(rate);
            }
        };

    }
}