package com.github.ApplicationConverter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class VolumeConverter extends JPanel {
    enum Convert {
        l("liter"),
        m3("m3"),
        gallon("gallon"),
        pint("pint"),
        quart("quart"),
        barrel("barrel"),
        cubic_foot("cubic_foot"),
        cubic_inch("cubic_inch");


        private String description;

        Convert(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return this.name() + " - " + this.description;
        }
    }

    class ConvertPair {
        private final Convert from;
        private final Convert to;

        public ConvertPair(Convert from, Convert to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ConvertPair that = (ConvertPair) o;
            if (from != that.from) return false;
            return to == that.to;
        }

        @Override
        public int hashCode() {
            int result = from.hashCode();
            result = 31 * result + to.hashCode();
            return result;
        }
    }

    private final Map<ConvertPair, BigDecimal> exchangeRates = new HashMap<ConvertPair, BigDecimal>() {
        {
            put(new ConvertPair(Convert.l, Convert.l), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.m3, Convert.m3), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.gallon, Convert.gallon), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.pint, Convert.pint), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.quart, Convert.quart), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.barrel, Convert.barrel), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.cubic_foot, Convert.cubic_foot), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.cubic_inch, Convert.cubic_inch), BigDecimal.valueOf(1));

            put(new ConvertPair(Convert.l, Convert.m3), BigDecimal.valueOf(0.001));
            put(new ConvertPair(Convert.l, Convert.gallon), BigDecimal.valueOf(0.219969248));
            put(new ConvertPair(Convert.l, Convert.pint), BigDecimal.valueOf(1.759753986));
            put(new ConvertPair(Convert.l, Convert.quart), BigDecimal.valueOf(0.879876993));
            put(new ConvertPair(Convert.l, Convert.barrel), BigDecimal.valueOf(0.006110256));
            put(new ConvertPair(Convert.l, Convert.cubic_foot), BigDecimal.valueOf(0.03531466));
            put(new ConvertPair(Convert.l, Convert.cubic_inch), BigDecimal.valueOf(61.0237));

            put(new ConvertPair(Convert.m3, Convert.l), BigDecimal.valueOf(1000));
            put(new ConvertPair(Convert.m3, Convert.gallon), BigDecimal.valueOf(219.9692482991));
            put(new ConvertPair(Convert.m3, Convert.pint), BigDecimal.valueOf(1759.753986393));
            put(new ConvertPair(Convert.m3, Convert.quart), BigDecimal.valueOf(879.8769931964));
            put(new ConvertPair(Convert.m3, Convert.barrel), BigDecimal.valueOf(6.110256897197));
            put(new ConvertPair(Convert.m3, Convert.cubic_foot), BigDecimal.valueOf(35.31466672149));
            put(new ConvertPair(Convert.m3, Convert.cubic_inch), BigDecimal.valueOf(61023.74409473));

            put(new ConvertPair(Convert.gallon, Convert.l), BigDecimal.valueOf(4.54609));
            put(new ConvertPair(Convert.gallon, Convert.m3), BigDecimal.valueOf(0.00454609));
            put(new ConvertPair(Convert.gallon, Convert.pint), BigDecimal.valueOf(8.0));
            put(new ConvertPair(Convert.gallon, Convert.quart), BigDecimal.valueOf(4.0));
            put(new ConvertPair(Convert.gallon, Convert.barrel), BigDecimal.valueOf(0.027777777));
            put(new ConvertPair(Convert.gallon, Convert.cubic_foot), BigDecimal.valueOf(0.160543653));
            put(new ConvertPair(Convert.gallon, Convert.cubic_inch), BigDecimal.valueOf(277.4194327916));

            put(new ConvertPair(Convert.pint, Convert.l), BigDecimal.valueOf(0.56826125));
            put(new ConvertPair(Convert.pint, Convert.m3), BigDecimal.valueOf(0.00056826125));
            put(new ConvertPair(Convert.pint, Convert.gallon), BigDecimal.valueOf(0.125));
            put(new ConvertPair(Convert.pint, Convert.quart), BigDecimal.valueOf(0.5));
            put(new ConvertPair(Convert.pint, Convert.barrel), BigDecimal.valueOf(0.0034722));
            put(new ConvertPair(Convert.pint, Convert.cubic_foot), BigDecimal.valueOf(0.0200679));
            put(new ConvertPair(Convert.pint, Convert.cubic_inch), BigDecimal.valueOf(34.6774290));

            put(new ConvertPair(Convert.quart, Convert.l), BigDecimal.valueOf(1.1365225));
            put(new ConvertPair(Convert.quart, Convert.m3), BigDecimal.valueOf(0.0011365225));
            put(new ConvertPair(Convert.quart, Convert.gallon), BigDecimal.valueOf(0.25));
            put(new ConvertPair(Convert.quart, Convert.pint), BigDecimal.valueOf(2.0));
            put(new ConvertPair(Convert.quart, Convert.barrel), BigDecimal.valueOf(0.006944444));
            put(new ConvertPair(Convert.quart, Convert.cubic_foot), BigDecimal.valueOf(0.040135913));
            put(new ConvertPair(Convert.quart, Convert.cubic_inch), BigDecimal.valueOf(69.3548581));

            put(new ConvertPair(Convert.barrel, Convert.l), BigDecimal.valueOf(163.65924));
            put(new ConvertPair(Convert.barrel, Convert.m3), BigDecimal.valueOf(0.16365924));
            put(new ConvertPair(Convert.barrel, Convert.gallon), BigDecimal.valueOf(36.0));
            put(new ConvertPair(Convert.barrel, Convert.pint), BigDecimal.valueOf(288.0));
            put(new ConvertPair(Convert.barrel, Convert.quart), BigDecimal.valueOf(144.0));
            put(new ConvertPair(Convert.barrel, Convert.cubic_foot), BigDecimal.valueOf(5.779571516));
            put(new ConvertPair(Convert.barrel, Convert.cubic_inch), BigDecimal.valueOf(9987.099580));

            put(new ConvertPair(Convert.cubic_foot, Convert.l), BigDecimal.valueOf(28.3168465));
            put(new ConvertPair(Convert.cubic_foot, Convert.m3), BigDecimal.valueOf(0.028316846));
            put(new ConvertPair(Convert.cubic_foot, Convert.gallon), BigDecimal.valueOf(6.2288354));
            put(new ConvertPair(Convert.cubic_foot, Convert.pint), BigDecimal.valueOf(49.8306836));
            put(new ConvertPair(Convert.cubic_foot, Convert.quart), BigDecimal.valueOf(24.9153418));
            put(new ConvertPair(Convert.cubic_foot, Convert.barrel), BigDecimal.valueOf(0.17302320));
            put(new ConvertPair(Convert.cubic_foot, Convert.cubic_inch), BigDecimal.valueOf(1728));

            put(new ConvertPair(Convert.cubic_inch, Convert.l), BigDecimal.valueOf(0.016387));
            put(new ConvertPair(Convert.cubic_inch, Convert.m3), BigDecimal.valueOf(0.0000163));
            put(new ConvertPair(Convert.cubic_inch, Convert.gallon), BigDecimal.valueOf(0.00360465));
            put(new ConvertPair(Convert.cubic_inch, Convert.pint), BigDecimal.valueOf(0.02883720));
            put(new ConvertPair(Convert.cubic_inch, Convert.quart), BigDecimal.valueOf(0.0144186005));
            put(new ConvertPair(Convert.cubic_inch, Convert.barrel), BigDecimal.valueOf(0.0001001291));
            put(new ConvertPair(Convert.cubic_inch, Convert.cubic_foot), BigDecimal.valueOf(0.000578703));

        }
    };

    public VolumeConverter() {
        super(new FlowLayout(FlowLayout.LEADING));

        // From
        JPanel from = new JPanel();
        JComboBox fromOptions = new JComboBox(Convert.values());
        from.add(fromOptions);
        from.setBorder(BorderFactory.createTitledBorder("Select Convert"));
        add(from, BorderLayout.CENTER);

        // Amount
        JTextField amountInput = new JTextField(20);
        JPanel amount = new JPanel();
        amount.add(amountInput);
        amount.setBorder(BorderFactory.createTitledBorder("Enter Ammount"));
        add(amount, BorderLayout.CENTER);

        // To
        JComboBox toOptions = new JComboBox(Convert.values());
        JPanel to = new JPanel();
        to.add(toOptions);
        to.setBorder(BorderFactory.createTitledBorder("Convert to"));
        add(to, BorderLayout.CENTER);

        // Convert Action
        JLabel convertText = new JLabel();
        JButton convertCmd = new JButton("Convert");
        convertCmd.addActionListener(convertAction(amountInput, fromOptions, toOptions, convertText));
        JPanel convert = new JPanel();
        convert.add(convertCmd);
        convert.add(convertText);
        add(convert);
    }

    private ActionListener convertAction(
            final JTextField amountInput,
            final JComboBox fromOptions,
            final JComboBox toOptions,
            final JLabel convertText) {

        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String amountInputText = amountInput.getText();
                if ("".equals(amountInputText)) {
                    return;
                }

                // Convert
                BigDecimal conversion = convertConvert(amountInputText);
                convertText.setText(conversion.toString());
            }

            private BigDecimal convertConvert(String amountInputText) {
                ConvertPair ConvertPair = new ConvertPair(
                        (Convert) fromOptions.getSelectedItem(),
                        (Convert) toOptions.getSelectedItem());
                BigDecimal rate = exchangeRates.get(ConvertPair);
                BigDecimal amount = new BigDecimal(amountInputText);
                return amount.multiply(rate);
            }
        };

    }
}
