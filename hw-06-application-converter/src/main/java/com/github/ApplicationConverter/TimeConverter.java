package com.github.ApplicationConverter;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class TimeConverter extends JPanel {
    enum Convert {
        sec("Second"),
        min("Minute"),
        hour("Hour"),
        day("Day"),
        week("Week"),
        month("Month"),
        ast_year("Astronomical Year");


        private String description;

        Convert(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return this.name() + " - " + this.description;
        }
    }

    class ConvertPair {
        private final Convert from;
        private final Convert to;

        public ConvertPair(Convert from, Convert to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ConvertPair that = (ConvertPair) o;
            if (from != that.from) return false;
            return to == that.to;
        }

        @Override
        public int hashCode() {
            int result = from.hashCode();
            result = 31 * result + to.hashCode();
            return result;
        }
    }

    private final Map<ConvertPair, BigDecimal> exchangeRates = new HashMap<ConvertPair, BigDecimal>() {
        {
            put(new ConvertPair(Convert.sec, Convert.sec), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.min, Convert.min), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.hour, Convert.hour), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.day, Convert.day), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.week, Convert.week), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.month, Convert.month), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.ast_year, Convert.ast_year), BigDecimal.valueOf(1));

            put(new ConvertPair(Convert.sec, Convert.min), BigDecimal.valueOf(0.0166667));
            put(new ConvertPair(Convert.sec, Convert.hour), BigDecimal.valueOf(0.000277778));
            put(new ConvertPair(Convert.sec, Convert.day), BigDecimal.valueOf(1.1574e-5));
            put(new ConvertPair(Convert.sec, Convert.week), BigDecimal.valueOf(1.6534285714e-6));
            put(new ConvertPair(Convert.sec, Convert.month), BigDecimal.valueOf(3.80514651484217549e-7));
            put(new ConvertPair(Convert.sec, Convert.ast_year), BigDecimal.valueOf(0.0));

            put(new ConvertPair(Convert.min, Convert.sec), BigDecimal.valueOf(60));
            put(new ConvertPair(Convert.min, Convert.hour), BigDecimal.valueOf(0.016666666));
            put(new ConvertPair(Convert.min, Convert.day), BigDecimal.valueOf(0.000694444));
            put(new ConvertPair(Convert.min, Convert.week), BigDecimal.valueOf(9.9206e-5));
            put(new ConvertPair(Convert.min, Convert.month), BigDecimal.valueOf(2.2831e-5));
            put(new ConvertPair(Convert.min, Convert.ast_year), BigDecimal.valueOf(0.0));

            put(new ConvertPair(Convert.hour, Convert.sec), BigDecimal.valueOf(3600));
            put(new ConvertPair(Convert.hour, Convert.min), BigDecimal.valueOf(60));
            put(new ConvertPair(Convert.hour, Convert.day), BigDecimal.valueOf(0.0416667));
            put(new ConvertPair(Convert.hour, Convert.week), BigDecimal.valueOf(0.00595238));
            put(new ConvertPair(Convert.hour, Convert.month), BigDecimal.valueOf(0.00136986));
            put(new ConvertPair(Convert.hour, Convert.ast_year), BigDecimal.valueOf(0.0));

            put(new ConvertPair(Convert.day, Convert.sec), BigDecimal.valueOf(86400));
            put(new ConvertPair(Convert.day, Convert.min), BigDecimal.valueOf(1440));
            put(new ConvertPair(Convert.day, Convert.hour), BigDecimal.valueOf(24));
            put(new ConvertPair(Convert.day, Convert.week), BigDecimal.valueOf(0.142857));
            put(new ConvertPair(Convert.day, Convert.month), BigDecimal.valueOf(0.0328767));
            put(new ConvertPair(Convert.day, Convert.ast_year), BigDecimal.valueOf(0.0));

            put(new ConvertPair(Convert.week, Convert.sec), BigDecimal.valueOf(604800));
            put(new ConvertPair(Convert.week, Convert.min), BigDecimal.valueOf(10080));
            put(new ConvertPair(Convert.week, Convert.hour), BigDecimal.valueOf(168));
            put(new ConvertPair(Convert.week, Convert.day), BigDecimal.valueOf(7));
            put(new ConvertPair(Convert.week, Convert.month), BigDecimal.valueOf(0.230137));
            put(new ConvertPair(Convert.week, Convert.ast_year), BigDecimal.valueOf(0.0));

            put(new ConvertPair(Convert.month, Convert.sec), BigDecimal.valueOf(2.628e+6));
            put(new ConvertPair(Convert.month, Convert.min), BigDecimal.valueOf(43800));
            put(new ConvertPair(Convert.month, Convert.hour), BigDecimal.valueOf(730.0));
            put(new ConvertPair(Convert.month, Convert.day), BigDecimal.valueOf(30.4167));
            put(new ConvertPair(Convert.month, Convert.week), BigDecimal.valueOf(4.34524));
            put(new ConvertPair(Convert.month, Convert.ast_year), BigDecimal.valueOf(0.0));

            put(new ConvertPair(Convert.ast_year, Convert.sec), BigDecimal.valueOf(31557600.0 ));
            put(new ConvertPair(Convert.ast_year, Convert.min), BigDecimal.valueOf(525960.0));
            put(new ConvertPair(Convert.ast_year, Convert.hour), BigDecimal.valueOf(8766.0));
            put(new ConvertPair(Convert.ast_year, Convert.day), BigDecimal.valueOf(365.25));
            put(new ConvertPair(Convert.ast_year, Convert.week), BigDecimal.valueOf(52.1429));
            put(new ConvertPair(Convert.ast_year, Convert.month), BigDecimal.valueOf(12.0));

        }
    };

    public TimeConverter() {
        super(new FlowLayout(FlowLayout.LEADING));

        // From
        JPanel from = new JPanel();
        JComboBox fromOptions = new JComboBox(Convert.values());
        from.add(fromOptions);
        from.setBorder(BorderFactory.createTitledBorder("Select Convert"));
        add(from, BorderLayout.CENTER);

        // Amount
        JTextField amountInput = new JTextField(20);
        JPanel amount = new JPanel();
        amount.add(amountInput);
        amount.setBorder(BorderFactory.createTitledBorder("Enter Ammount"));
        add(amount, BorderLayout.CENTER);

        // To
        JComboBox toOptions = new JComboBox(Convert.values());
        JPanel to = new JPanel();
        to.add(toOptions);
        to.setBorder(BorderFactory.createTitledBorder("Convert to"));
        add(to, BorderLayout.CENTER);

        // Convert Action
        JLabel convertText = new JLabel();
        JButton convertCmd = new JButton("Convert");
        convertCmd.addActionListener(convertAction(amountInput, fromOptions, toOptions, convertText));
        JPanel convert = new JPanel();
        convert.add(convertCmd);
        convert.add(convertText);
        add(convert);
    }

    private ActionListener convertAction(
            final JTextField amountInput,
            final JComboBox fromOptions,
            final JComboBox toOptions,
            final JLabel convertText) {

        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String amountInputText = amountInput.getText();
                if ("".equals(amountInputText)) {
                    return;
                }

                // Convert
                BigDecimal conversion = convertConvert(amountInputText);
                convertText.setText(conversion.toString());
            }

            private BigDecimal convertConvert(String amountInputText) {
                ConvertPair ConvertPair = new ConvertPair(
                        (Convert) fromOptions.getSelectedItem(),
                        (Convert) toOptions.getSelectedItem());
                BigDecimal rate = exchangeRates.get(ConvertPair);
                BigDecimal amount = new BigDecimal(amountInputText);
                return amount.multiply(rate);
            }
        };

    }

}
