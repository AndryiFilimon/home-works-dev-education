package com.github.ApplicationConverter;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ApplicationConverter {

    public static class WeightConverter extends JPanel {
        enum Convert {
            g("gram"),
            kg("kilogram"),
            carat("carat"),
            eng_pound("english pound"),
            pound("pound"),
            stone("stone"),
            rus_pound("russian pound");


            private String description;

            Convert(String description) {
                this.description = description;
            }

            @Override
            public String toString() {
                return this.name() + " - " + this.description;
            }
        }

        class ConvertPair {
            private final Convert from;
            private final Convert to;

            public ConvertPair(Convert from, Convert to) {
                this.from = from;
                this.to = to;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                ConvertPair that = (ConvertPair) o;
                if (from != that.from) return false;
                return to == that.to;
            }

            @Override
            public int hashCode() {
                int result = from.hashCode();
                result = 31 * result + to.hashCode();
                return result;
            }
        }

        private final Map<ConvertPair, BigDecimal> exchangeRates = new HashMap<ConvertPair, BigDecimal>() {
            {
                put(new ConvertPair(Convert.g, Convert.g), BigDecimal.valueOf(1));
                put(new ConvertPair(Convert.kg, Convert.kg), BigDecimal.valueOf(1));
                put(new ConvertPair(Convert.carat, Convert.carat), BigDecimal.valueOf(1));
                put(new ConvertPair(Convert.eng_pound, Convert.eng_pound), BigDecimal.valueOf(1));
                put(new ConvertPair(Convert.pound, Convert.pound), BigDecimal.valueOf(1));
                put(new ConvertPair(Convert.stone, Convert.stone), BigDecimal.valueOf(1));
                put(new ConvertPair(Convert.rus_pound, Convert.rus_pound), BigDecimal.valueOf(1));

                put(new ConvertPair(Convert.g, Convert.kg), BigDecimal.valueOf(0.001));
                put(new ConvertPair(Convert.g, Convert.carat), BigDecimal.valueOf(5));
                put(new ConvertPair(Convert.g, Convert.eng_pound), BigDecimal.valueOf(0.0022));
                put(new ConvertPair(Convert.g, Convert.pound), BigDecimal.valueOf(0.0022));
                put(new ConvertPair(Convert.g, Convert.stone), BigDecimal.valueOf(0.0001));
                put(new ConvertPair(Convert.g, Convert.rus_pound), BigDecimal.valueOf(0.0024));

                put(new ConvertPair(Convert.kg, Convert.g), BigDecimal.valueOf(1000));
                put(new ConvertPair(Convert.kg, Convert.carat), BigDecimal.valueOf(5000));
                put(new ConvertPair(Convert.kg, Convert.eng_pound), BigDecimal.valueOf(2.2046));
                put(new ConvertPair(Convert.kg, Convert.pound), BigDecimal.valueOf(2.2046));
                put(new ConvertPair(Convert.kg, Convert.stone), BigDecimal.valueOf(0.1574));
                put(new ConvertPair(Convert.kg, Convert.rus_pound), BigDecimal.valueOf(2.4419));

                put(new ConvertPair(Convert.carat, Convert.g), BigDecimal.valueOf(0.2));
                put(new ConvertPair(Convert.carat, Convert.kg), BigDecimal.valueOf(0.0002));
                put(new ConvertPair(Convert.carat, Convert.eng_pound), BigDecimal.valueOf(0.0004));
                put(new ConvertPair(Convert.carat, Convert.pound), BigDecimal.valueOf(0.0004));
                put(new ConvertPair(Convert.carat, Convert.stone), BigDecimal.valueOf(0.00003));
                put(new ConvertPair(Convert.carat, Convert.rus_pound), BigDecimal.valueOf(0.0004));

                put(new ConvertPair(Convert.eng_pound, Convert.g), BigDecimal.valueOf(453.5923));
                put(new ConvertPair(Convert.eng_pound, Convert.kg), BigDecimal.valueOf(0.4535));
                put(new ConvertPair(Convert.eng_pound, Convert.carat), BigDecimal.valueOf(2267.9618));
                put(new ConvertPair(Convert.eng_pound, Convert.pound), BigDecimal.valueOf(1));
                put(new ConvertPair(Convert.eng_pound, Convert.stone), BigDecimal.valueOf(0.0714));
                put(new ConvertPair(Convert.eng_pound, Convert.rus_pound), BigDecimal.valueOf(1.1076));

                put(new ConvertPair(Convert.pound, Convert.g), BigDecimal.valueOf(453.5923));
                put(new ConvertPair(Convert.pound, Convert.kg), BigDecimal.valueOf(0.4535));
                put(new ConvertPair(Convert.pound, Convert.carat), BigDecimal.valueOf(2267.9618));
                put(new ConvertPair(Convert.pound, Convert.eng_pound), BigDecimal.valueOf(1));
                put(new ConvertPair(Convert.pound, Convert.stone), BigDecimal.valueOf(0.0714));
                put(new ConvertPair(Convert.pound, Convert.rus_pound), BigDecimal.valueOf(1.1076));

                put(new ConvertPair(Convert.stone, Convert.g), BigDecimal.valueOf(6350.2933));
                put(new ConvertPair(Convert.stone, Convert.kg), BigDecimal.valueOf(6.3502));
                put(new ConvertPair(Convert.stone, Convert.carat), BigDecimal.valueOf(31751.4667));
                put(new ConvertPair(Convert.stone, Convert.pound), BigDecimal.valueOf(14.0));
                put(new ConvertPair(Convert.stone, Convert.eng_pound), BigDecimal.valueOf(14.0));
                put(new ConvertPair(Convert.stone, Convert.rus_pound), BigDecimal.valueOf(15.5069));

                put(new ConvertPair(Convert.rus_pound, Convert.g), BigDecimal.valueOf(409.512));
                put(new ConvertPair(Convert.rus_pound, Convert.kg), BigDecimal.valueOf(0.4095));
                put(new ConvertPair(Convert.rus_pound, Convert.carat), BigDecimal.valueOf(2047.5601));
                put(new ConvertPair(Convert.rus_pound, Convert.pound), BigDecimal.valueOf(0.9028));
                put(new ConvertPair(Convert.rus_pound, Convert.eng_pound), BigDecimal.valueOf(0.9028));
                put(new ConvertPair(Convert.rus_pound, Convert.stone), BigDecimal.valueOf(0.0644));

            }
        };

        public WeightConverter() {
            super(new FlowLayout(FlowLayout.LEADING));

            // From
            JPanel from = new JPanel();
            JComboBox fromOptions = new JComboBox(Convert.values());
            from.add(fromOptions);
            from.setBorder(BorderFactory.createTitledBorder("Select Convert"));
            add(from, BorderLayout.CENTER);

            // Amount
            JTextField amountInput = new JTextField(20);
            JPanel amount = new JPanel();
            amount.add(amountInput);
            amount.setBorder(BorderFactory.createTitledBorder("Enter Ammount"));
            add(amount, BorderLayout.CENTER);

            // To
            JComboBox toOptions = new JComboBox(Convert.values());
            JPanel to = new JPanel();
            to.add(toOptions);
            to.setBorder(BorderFactory.createTitledBorder("Convert to"));
            add(to, BorderLayout.CENTER);

            // Convert Action
            JLabel convertText = new JLabel();
            JButton convertCmd = new JButton("Convert");
            convertCmd.addActionListener(convertAction(amountInput, fromOptions, toOptions, convertText));
            JPanel convert = new JPanel();
            convert.add(convertCmd);
            convert.add(convertText);
            add(convert);
        }

        private ActionListener convertAction(
                final JTextField amountInput,
                final JComboBox fromOptions,
                final JComboBox toOptions,
                final JLabel convertText) {

            return new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String amountInputText = amountInput.getText();
                    if ("".equals(amountInputText)) {
                        return;
                    }

                    // Convert
                    BigDecimal conversion = convertConvert(amountInputText);
                    convertText.setText(conversion.toString());
                }

                private BigDecimal convertConvert(String amountInputText) {
                    ConvertPair ConvertPair = new ConvertPair(
                            (Convert) fromOptions.getSelectedItem(),
                            (Convert) toOptions.getSelectedItem());
                    BigDecimal rate = exchangeRates.get(ConvertPair);
                    BigDecimal amount = new BigDecimal(amountInputText);
                    return amount.multiply(rate);
                }
            };
        }
    }
}