package com.github.ObjOrProg;

public class ProcessorX86 extends Processor{

    public final String architecture = "X86";

    public ProcessorX86(double frequency, double cache, double bitCapacity, String architecture) {
        super(frequency, cache, bitCapacity, architecture);
    }

    @Override
    public String dataProcess(String data) {
        return data.toLowerCase();
    }

    @Override
    public String dataProcess(long data) {
        return Long.toString(data);
    }
}
