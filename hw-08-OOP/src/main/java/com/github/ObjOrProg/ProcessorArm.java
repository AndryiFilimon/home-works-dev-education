package com.github.ObjOrProg;

public class ProcessorArm extends Processor {

    final String architecture = "ARM";

    public ProcessorArm(double frequency, double cache, double bitCapacity, String architecture) {
        super(frequency, cache, bitCapacity, architecture);
    }

    @Override
    public String dataProcess(String data) {
        return data.toUpperCase();
    }

    @Override
    public String dataProcess(long data) {
        return Long.toString(data);
    }
}
