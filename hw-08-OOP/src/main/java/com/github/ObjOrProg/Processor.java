package com.github.ObjOrProg;

public abstract class Processor<Srting> {
    private double frequency;
    private double cache;
    private double bitCapacity;
    public String architecture;

    public Processor(double frequency, double cache, double bitCapacity, String architecture) {
        this.frequency = frequency;
        this.cache = cache;
        this.bitCapacity = bitCapacity;
        this.architecture = architecture;
    }

    public double getFrequency() {
        return frequency;
    }

    public double getCache() {
        return cache;
    }

    public double getBitCapacity() { return bitCapacity; }

    public String getArchitecture() { return architecture; }

    public String getDetails() {
        return Double.toString(getFrequency()) + "\n" + Double.toString(getCache())
                + "\n" + Double.toString(getBitCapacity()) + "\n" + getArchitecture();
    }

    public abstract String dataProcess(String data);

    public abstract String dataProcess(long data);

}
