package com.github.ObjOrProg;

import java.util.ArrayList;

public class DeviceFilter {

    public static Device[] ProcessorFilter(String processorType, Device[] devices) {
      ArrayList<Device> result = new ArrayList<>();
        for (int i = 0; i < devices.length; i++) {
            if (devices[i].getProcessor().architecture == processorType) {
                result.add(devices[i]);
            }
        }
        Device[] resultArray = new Device[result.size()];
        for (int i = 0; i < resultArray.length; i++) {
            resultArray[i] = result.get(i);
        }
        return resultArray;
    }

    public static Device[] ProcessorFilterFrequency(double processorParameter, Device[] devices) {
       ArrayList<Device> result = new ArrayList<>();
        for (int i = 0; i < devices.length; i++) {
            if (devices[i].getProcessor().getFrequency() >= processorParameter) {
                result.add(devices[i]);
            }
        }
        Device[] resultArray = new Device[result.size()];
        for (int i = 0; i < resultArray.length; i++) {
            resultArray[i] = result.get(i);
        }
        return resultArray;
    }

    public static Device[] ProcessorFilterCache(double processorParameter, Device[] devices) {
       ArrayList<Device> result = new ArrayList<>();
        for (int i = 0; i < devices.length; i++) {
            if (devices[i].getProcessor().getCache() >= processorParameter) {
                result.add(devices[i]);
            }
        }
            Device[] resultArray = new Device[result.size()];
            for (int j = 0; j < resultArray.length; j++) {
                resultArray[j] = result.get(j);
            }
        return resultArray;
    }

    public static Device[] ProcessorFilterBitCapacity(double processorParameter, Device[] devices) {
        ArrayList<Device> result = new ArrayList();
        for (int i = 0; i < devices.length; i++) {
            if (devices[i].getProcessor().getBitCapacity() >= processorParameter) {
                result.add(devices[i]);
            }
        }
        Device[] resultArray = new Device[result.size()];
        for (int j = 0; j < resultArray.length; j++) {
            resultArray[j] = result.get(j);
        }
        return resultArray;
    }


    public static Device[] MemoryVolumeFilter(int memoryVolume, Device[] devices) {
        ArrayList<Device> result = new ArrayList();
        for (int i = 0; i < devices.length; i++) {
            if (devices[i].getMemory().getMemoryInfo().availableSlots >= memoryVolume) {
                result.add(devices[i]);
            }
        }
        Device[] resultArray = new Device[result.size()];

        for (int i = 0; i < resultArray.length; i++) {
            resultArray[i] = result.get(i);
        }
        return resultArray;

    }

    public static Device[] UsedMemoryVolumeFilter(double usedMemoryVolume, Device[] devices) {
        ArrayList<Device> result = new ArrayList();
        for (int i = 0; i < devices.length; i++) {
            if (devices[i].getMemory().getMemoryInfo().availableSlotsPercent >= usedMemoryVolume) {
                result.add(devices[i]);
            }
        }
        Device[] resultArray = new Device[result.size()];

        for (int i = 0; i < resultArray.length; i++) {
            resultArray[i] = result.get(i);
        }
        return resultArray;
    }
}
