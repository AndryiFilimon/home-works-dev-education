package com.github.ObjOrProg;

public class Memory {

    int[] memoryCell;
    MemoryInfo memoryInfo;

    public Memory(int[] memoryCell, MemoryInfo memoryInfo) {
        this.memoryCell = memoryCell;
        this.memoryInfo = memoryInfo;
    }

    public int readLast(int[] read) {
        int strin = 0;
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != 0) {
                strin = memoryCell[i];
                break;
            }
        }
        return strin;
    }

    public int removeLast() {
        int strin = 0;
        for (int i = memoryCell.length - 1; i >= 0; i--) {
            if (memoryCell[i] != 0) {
                memoryCell[i] = 0;
                strin = memoryCell[i];
                break;
            }
        }
        return strin;
    }

    public boolean save() {
        boolean m = false;
        for (int i = 0; i < memoryCell.length; i++) {
            if (memoryCell[i] == 0) m = true;
            else {
                m = false;
            }
        }
        return m;
    }

    public MemoryInfo getMemoryInfo() {
        return memoryInfo;
    }


    }

class MemoryInfo {
    public int availableSlots;
    public double availableSlotsPercent;

    public MemoryInfo(int availableSlots, double availableSlotsPercent) {
        this.availableSlots = availableSlots;
        this.availableSlotsPercent = availableSlotsPercent;
    }

    @Override
    public String toString() {
        return "MemoryInfo{" +
                "availableSlots=" + availableSlots +
                ", availableSlotsPercent='" + availableSlotsPercent + "%" + '\'' +
                '}';
    }
}
