package com.github.ObjOrProg;

import org.junit.Assert;
import org.junit.Test;

public class TestUnit {
    Memory memory;

    @Test

    public void UsedMemoryVolumeFilterTest() {
        Device[] devices = new Device[]{
                new Device(new ProcessorArm(200.0, 300.0, 100.0, "ARM"), new Memory(new int[]{1000, 2000, 3000, 5000}, new MemoryInfo(4, 50.0))),
                new Device(new ProcessorArm(300.0, 200.0, 300.0, "ARM"), new Memory(new int[]{3000, 3000, 5000}, new MemoryInfo(6, 50.0))),
                new Device(new ProcessorX86(400.0, 500.0, 200.0, "X86"), new Memory(new int[]{6000, 6000}, new MemoryInfo(4, 20.0))),
                new Device(new ProcessorX86(200.0, 400.0, 400.0, "X86"), new Memory(new int[]{12000}, new MemoryInfo(10, 80.0))),
        };
        int act = 3;
        Device[] result = DeviceFilter.UsedMemoryVolumeFilter(50.0, devices);
        int exp = result.length;
        Assert.assertEquals(exp, act);
    }

    @Test

    public void MemoryVolumeFilterTest() {
        Device[] devices = new Device[]{
                new Device(new ProcessorArm(200.0, 300.0, 100.0, "ARM"), new Memory(new int[]{1000, 2000, 3000, 5000}, new MemoryInfo(4, 50.0))),
                new Device(new ProcessorArm(300.0, 200.0, 300.0, "ARM"), new Memory(new int[]{3000, 3000, 5000}, new MemoryInfo(6, 50.0))),
                new Device(new ProcessorX86(400.0, 500.0, 200.0, "X86"), new Memory(new int[]{6000, 6000}, new MemoryInfo(4, 20.0))),
                new Device(new ProcessorX86(200.0, 400.0, 400.0, "X86"), new Memory(new int[]{12000}, new MemoryInfo(10, 80.0))),
        };
        int act = 2;
        Device[] result = DeviceFilter.MemoryVolumeFilter(5, devices);
        int exp = result.length;
        Assert.assertEquals(exp, act);
    }

    @Test

    public void ProcessorFilterBitCapacityTest() {
        Device[] devices = new Device[]{
                new Device(new ProcessorArm(200.0, 300.0, 100.0, "ARM"), new Memory(new int[]{1000, 2000, 3000, 5000}, new MemoryInfo(4, 50.0))),
                new Device(new ProcessorArm(300.0, 200.0, 300.0, "ARM"), new Memory(new int[]{3000, 3000, 5000}, new MemoryInfo(6, 50.0))),
                new Device(new ProcessorX86(400.0, 500.0, 200.0, "X86"), new Memory(new int[]{6000, 6000}, new MemoryInfo(4, 20.0))),
                new Device(new ProcessorX86(200.0, 400.0, 400.0, "X86"), new Memory(new int[]{12000}, new MemoryInfo(10, 80.0))),
        };
        int act = 2;
        Device[] result = DeviceFilter.ProcessorFilterBitCapacity(250, devices);
        int exp = result.length;
        Assert.assertEquals(exp, act);
    }

    @Test

    public void ProcessorFilterCacheTest() {
        Device[] devices = new Device[]{
                new Device(new ProcessorArm(200.0, 300.0, 100.0, "ARM"), new Memory(new int[]{1000, 2000, 3000, 5000}, new MemoryInfo(4, 50.0))),
                new Device(new ProcessorArm(300.0, 200.0, 300.0, "ARM"), new Memory(new int[]{3000, 3000, 5000}, new MemoryInfo(6, 50.0))),
                new Device(new ProcessorX86(400.0, 500.0, 200.0, "X86"), new Memory(new int[]{6000, 6000}, new MemoryInfo(4, 20.0))),
                new Device(new ProcessorX86(200.0, 400.0, 400.0, "X86"), new Memory(new int[]{12000}, new MemoryInfo(10, 80.0))),
        };
        int act = 3;
        Device[] result = DeviceFilter.ProcessorFilterCache(256, devices);
        int exp = result.length;
        Assert.assertEquals(exp, act);
    }

    @Test

    public void ProcessorFilterFrequencyTest() {
        Device[] devices = new Device[]{
                new Device(new ProcessorArm(200.0, 300.0, 100.0, "ARM"), new Memory(new int[]{1000, 2000, 3000, 5000}, new MemoryInfo(4, 50.0))),
                new Device(new ProcessorArm(300.0, 200.0, 300.0, "ARM"), new Memory(new int[]{3000, 3000, 5000}, new MemoryInfo(6, 50.0))),
                new Device(new ProcessorX86(400.0, 500.0, 200.0, "X86"), new Memory(new int[]{6000, 6000}, new MemoryInfo(4, 20.0))),
                new Device(new ProcessorX86(200.0, 400.0, 400.0, "X86"), new Memory(new int[]{12000}, new MemoryInfo(10, 80.0))),
        };
        int act = 2;
        Device[] result = DeviceFilter.ProcessorFilterFrequency(250, devices);
        int exp = result.length;
        Assert.assertEquals(exp, act);
    }

    @Test

    public void ProcessorFilterTest() {
        Device[] devices = new Device[]{
                new Device(new ProcessorArm(200.0, 300.0, 100.0, "ARM"), new Memory(new int[]{1000, 2000, 3000, 5000}, new MemoryInfo(4, 50.0))),
                new Device(new ProcessorArm(300.0, 200.0, 300.0, "ARM"), new Memory(new int[]{3000, 3000, 5000}, new MemoryInfo(6, 50.0))),
                new Device(new ProcessorX86(400.0, 500.0, 200.0, "X86"), new Memory(new int[]{6000, 6000}, new MemoryInfo(4, 20.0))),
                new Device(new ProcessorX86(200.0, 400.0, 400.0, "X86"), new Memory(new int[]{12000}, new MemoryInfo(10, 80.0))),
        };
        int act = 2;
        Device[] result = DeviceFilter.ProcessorFilter("ARM", devices);
        int exp = result.length;
        Assert.assertEquals(exp, act);
    }

    @Test

    public void readLastTest(){
        int[] read = new int[]{1,5,7};
        int act = 2;
        int result = memory.readLast(read);
        int exp = read.length;
        Assert.assertEquals(act, exp);
    }

        }