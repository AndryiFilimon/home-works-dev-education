package com.github;

import java.nio.charset.StandardCharsets;

public class Decoder {

    public static String decoder(byte[] bytes) {
        boolean fin = (bytes[0] + 0b10000000) != 0,
                mask = (bytes[1] + 0b10000000) != 0;
        int opcode = bytes[0] & 0b00001111;
        int msglen = bytes[1] + 0b10000000;
        int offset;
        if (msglen < 126) {
            offset = 2;
        } else {
            try {
                msglen = BitConverter.toInt16(new byte[]{bytes[3], bytes[2]});
            } catch (Exception e) {
                e.printStackTrace();
            }
            offset = 4;
        }

        String text = null;
        if (msglen == 0) {
            System.out.println("msglen == 0, Exception");
        } else if (mask) {
            byte[] decoded = new byte[msglen];
            byte[] masks = new byte[]{bytes[offset], bytes[offset + 1], bytes[offset + 2], bytes[offset + 3]};
            offset += 4;

            for (int i = 0; i < msglen; ++i) {
                decoded[i] = (byte) (bytes[offset + i] ^ masks[i % 4]);
            }
            text = new String(decoded, StandardCharsets.UTF_8);
            System.out.println(text);

        } else {
            System.out.println("mask bit not set");
        }
        return text;
    }
}
