package com.github.app;

public class Main {

    public static void main(String[] args) {
        HttpHandler httpHandler = new HttpHandler(8085);
        Thread socketThread = new Thread(httpHandler, "Thread: ");
        socketThread.start();
        System.out.println(socketThread.getName() + " run");
    }

}
