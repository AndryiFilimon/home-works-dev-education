package com.github.app;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HttpHandler implements Runnable {

    private int port;

    private ServerSocket serverSocket;

    protected Thread runThread;

    private ExecutorService threadPool;

    public HttpHandler() {

    }

    public HttpHandler(int port) {
        this.port = port;
    }

    public void executeMethod(HttpRequest req, HttpResponse resp) throws IOException {
        switch (req.getMethod()) {
            case "GET":
                this.doGet(req, resp);
                break;
            case "HEAD":
                this.doHead(req, resp);
                break;
            case "POST":
                this.doPost(req, resp);
                break;
            case "PUT":
                this.doPut(req, resp);
                break;
            case "DELETE":
                this.doDelete(req, resp);
                break;
            case "CONNECT":
                this.doConnect(req, resp);
                break;
            case "OPTIONS":
                this.doOptions(req, resp);
                break;
            case "TRACE":
                this.doTrace(req, resp);
                break;
            case "PATCH":
                this.doPatch(req, resp);
                break;
            default:
                throw new IOException("Method " + req.getMethod() + " is allowed.");
        }
    }

    public void doGet(HttpRequest req, HttpResponse resp) {
        String outString = "<h1>doGet</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.addHeaders("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doPost(HttpRequest req, HttpResponse resp) {
        String outString = "<h1>doPost</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.addHeaders("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doHead(HttpRequest req, HttpResponse resp) {
        String outString = "<h1>doHead</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.addHeaders("Content-Type", "text/html");
    }

    public void doPut(HttpRequest req, HttpResponse resp) {
        String outString = "<h1>doPut</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.addHeaders("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doDelete(HttpRequest req, HttpResponse resp) {
        String outString = "<h1>doDelete</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.addHeaders("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doConnect(HttpRequest req, HttpResponse resp) {
        String outString = "<h1>doConnect</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.addHeaders("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doOptions(HttpRequest req, HttpResponse resp) {
        String outString = "<h1>doOptions</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.addHeaders("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doTrace(HttpRequest req, HttpResponse resp) {
        String outString = "<h1>doTrace</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.addHeaders("Content-Type", "text/html");
        resp.setBody(outString);
    }

    public void doPatch(HttpRequest req, HttpResponse resp) {
        String outString = "<h1>doPatch</h1>\n";
        resp.setProtocol("HTTP/1.1");
        resp.setStatusCode(200);
        resp.setStatusText("OK");
        resp.addHeaders("Content-Length", "" + outString.length());
        resp.addHeaders("Content-Type", "text/html");
        resp.setBody(outString);
    }

    @Override
    public void run() {
        String runThreadName;
        synchronized (this) {
            this.runThread = Thread.currentThread();
            this.threadPool = Executors.newFixedThreadPool(10);
            runThreadName = runThread.getName();
        }
        try {
            this.serverSocket = new ServerSocket(this.port);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int countThread = 0;
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                HttpRequest req = new HttpRequest();
                HttpResponse resp = new HttpResponse();
                req.takeRequest(socket);
                this.threadPool.execute(new HttpHandler());
                this.executeMethod(req, resp);
                resp.sendResponse(socket);
                this.runThread = new Thread(runThreadName + "" + countThread++);
                System.out.println(this.runThread.getName());
            } catch (IOException ignore) {
            }
        }
    }
}
