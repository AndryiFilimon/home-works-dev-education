package com.github.log4jRandom;

public class NumberException extends  Exception{

    public NumberException(String message){
        super(message);
    }
}
