package com.github.log4jRandom;

import java.util.logging.Logger;

public class RandomNum {

    private static final Logger log = Logger.getLogger(String.valueOf(RandomNum.class));

    public void randomNumber() throws NumberException {
        int a = 0;
        int b = 10;
        int result = (int) ((Math.random() * (b - a)) + a);
        if (result <= 5) {
            log.info("Exception number");
            throw new NumberException("Generated number " + result);
        } else {
            System.out.println("Application launched successfully");
            log.info("Loaded correctly");
        }
    }
}
