package com.github.balls;

import javax.swing.*;
import java.awt.*;

public class BallFrame extends JFrame {

    public BallFrame() throws HeadlessException {

        setVisible(Boolean.TRUE);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(800, 700);
        BallPanel ballPanel = new BallPanel();
        ballPanel.setBounds(0, 0, 750, 650);
        ballPanel.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f)));
        add(ballPanel);

    }
}
