package com.github.balls;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Ball extends JPanel implements Runnable {

    private int dx;
    private int dy;
    private final Color color;
    private final Point point;

    Random rand = new Random();
    int r = rand.nextInt(255);
    int g = rand.nextInt(255);
    int b = rand.nextInt(255);
    Color newColor = new Color(r, g, b);

    int height = rand.nextInt(Math.max(70,150));
    int width = height;

    public Ball(Point point) {
        this.point = point;
        color = newColor;
        dx = rand.nextInt(15) - 5;
        dy = rand.nextInt(15) - 5;
        setSize(height, width);
        setOpaque(Boolean.FALSE);
    }


    public void move() {
        JPanel pan = (JPanel) getParent();
        if (this.point.x <= 0 || this.point.x + width >= pan.getWidth()) {
            dx = -dx;
        }
        if (this.point.y <= 0 || this.point.y + height >= pan.getHeight()) {
            dy = -dy;
        }
        this.point.translate(dx, dy);
        setLocation(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(newColor);
        g2d.fillOval(0, 0, width, height);
    }

    @Override
    public void run() {
        try {
            while (true) {
                move();
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
