package com.github.balls;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class BallPanel extends JPanel {

public BallPanel(){
    setLayout(null);
    addMouseListener(new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent e) {

        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {
            Ball ball = new Ball(e.getPoint());
            add(ball);
            Thread tr = new Thread(ball);
            tr.start();
        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    });
    setVisible(Boolean.TRUE);
}
}
