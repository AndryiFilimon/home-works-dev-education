package com.github.sw;

import java.awt.*;
import javax.swing.*;
import java.util.Random;

public class Balls extends JPanel implements Runnable {

    Point point;
    Color color;
    private int vx;
    private int vy;

    Random random = new Random();
    int r = random.nextInt(255);
    int g = random.nextInt(255);
    int b = random.nextInt(255);
    Color newColor = new Color(r, g, b);

    int width = random.nextInt(Math.max(70, 150));
    int height = width;

    public Balls(Point point) {
        this.point = point;
        color = newColor;
        vx = random.nextInt(15) - 5;
        vy = random.nextInt(15) - 5;
        setSize(width, height);
        setOpaque(Boolean.FALSE);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(newColor);
        g2.fillOval(0, 0, width, height);
        g2.setColor(Color.black);
        g2.drawOval(0, 0, width, height);
    }

    public void run() {
        while (true) {
            try {
                move();
                Thread.sleep(25);
            } catch (InterruptedException e) {
                System.out.println("interrupted");
            }
        }
    }

    public void move() {
        if (this.point.x + vx < 0 || this.point.x + width + vx > getWidth()) {
            vx = -1;
        }
        if (this.point.y + vy < 0 || this.point.y + height + vy > getHeight()) {
            vy = -1;
        }
        this.point.translate(vx, vy);
        setLocation(point);
    }

}
