package com.github.sw;

import javax.swing.*;
import java.awt.*;

public class SwingPanel extends JFrame {

    public SwingPanel() throws HeadlessException {

        setVisible(Boolean.TRUE);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(800, 700);
        setLayout(null);
        BallsMetMouse ballsMetMouse = new BallsMetMouse();
        ballsMetMouse.setBounds(0, 0, 780, 680);
        ballsMetMouse.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f)));
        add(ballsMetMouse);
    }
}
