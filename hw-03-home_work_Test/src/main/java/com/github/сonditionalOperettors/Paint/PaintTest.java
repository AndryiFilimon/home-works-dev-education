package com.github.сonditionalOperettors.Paint;

import org.junit.Assert;
import org.junit.Test;

public class PaintTest {

    @Test
    public void paintTest() {
        String exp = "1";
        String act = String.valueOf(Paint.PaintResult(4, 4));
        Assert.assertEquals(exp, act);
    }
}