package com.github.сonditionalOperettors.Paint;

import java.util.Scanner;

public class Paint {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter number for y: ");
        int a = sc.nextInt();
        System.out.print("Enter number for x: ");
        int b = sc.nextInt();
        String result = String.valueOf(Paint.PaintResult(a, b));
        System.out.print(result);
    }

    public static int PaintResult(int x, int y) {
        int result = 0;
        if (x > 0 && y > 0) {
            result = 1;
        } else if (x < 0 && y > 0) {
            result = 2;
        } else if (x < 0 && y < 0) {
            result = 3;
        } else if (x > 0 && y < 0) {
            result = 4;
        }
        return result;
    }
}
