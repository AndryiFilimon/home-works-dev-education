package com.github.сonditionalOperettors.ThreeNumbers;

import java.util.Scanner;

public class ThreeNumbers {

    public static void main(String[] args) {
        Scanner cs = new Scanner(System.in);
        System.out.println("Введите число А");
        int a = cs.nextInt();
        System.out.println("Введите число B");
        int b = cs.nextInt();
        System.out.println("Введите число C");
        int c = cs.nextInt();
        int result = PositiveNumbersSum(a,b,c);

        System.out.println(result);
    }
    public static int PositiveNumbersSum(int a, int b, int c) {
        int result = 0;
        if(a>0) result += a;
        if(b>0) result += b;
        if (c>0) result += c;

       return result;
    }
}

