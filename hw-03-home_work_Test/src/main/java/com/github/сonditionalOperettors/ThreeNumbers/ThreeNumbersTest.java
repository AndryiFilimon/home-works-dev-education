package com.github.сonditionalOperettors.ThreeNumbers;
import org.junit.Assert;
import org.junit.Test;

public class ThreeNumbersTest {
    @Test
    public void NumbersThreeTest() {
        int exp = 6;
        int act = ThreeNumbers.PositiveNumbersSum(2,2,2);
        Assert.assertEquals(exp,act);
    }
}
