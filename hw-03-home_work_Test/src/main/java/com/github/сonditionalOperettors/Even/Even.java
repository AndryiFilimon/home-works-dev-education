package com.github.сonditionalOperettors.Even;

public class Even {

        public static void main (String[] args){

            int x = 98;
            int y = 3;
            int result = EvenResult(x,y);
            System.out.println("You result: " + result);
        }
        public static int EvenResult(int a, int b) {
            int result;
            if ((a % 2) != 0) {
                result = a + b;
            } else {
                result = a * b;
            }
            return result;
        }
    }

