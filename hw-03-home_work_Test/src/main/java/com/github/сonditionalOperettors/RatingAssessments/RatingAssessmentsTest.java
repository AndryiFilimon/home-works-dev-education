package com.github.сonditionalOperettors.RatingAssessments;

import org.junit.Assert;
import org.junit.Test;

import static com.github.сonditionalOperettors.RatingAssessments.RatingAssessments.RatingAssessmentsResult;

public class RatingAssessmentsTest {
    @Test
    public void ratingAssessmentsTest(){
        String exp = "D";
        String act = RatingAssessmentsResult(50);
        Assert.assertEquals(exp,act);
    }
}
