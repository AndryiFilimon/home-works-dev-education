package com.github.сonditionalOperettors.RatingAssessments;

import java.util.Scanner;


public class RatingAssessments {

    public static void main(String[] agrs) {

        System.out.print("Enter number: ");
        Scanner scr = new Scanner(System.in);
        int score = scr.nextInt();
        String result = RatingAssessmentsResult(score);
        System.out.println(result);
    }

    public static String RatingAssessmentsResult(int rating) {

        if (rating <= 19) {
        return "F";
        } else if (rating >= 20 && rating <= 39) {
            return "E";
        } else if (rating >= 40 && rating <= 59) {
            return "D";
        } else if (rating >= 60 && rating <= 74) {
            return "C";
        } else if (rating >= 75 && 89 >= rating) {
            return "B";
        } else if (rating >= 90 && rating <= 100) {
            return "A";
        }
        return "Error";

    }
}
