package com.github.сonditionalOperettors.VirMax;

import org.junit.Assert;
import org.junit.Test;

public class VirMaxTest {

    @Test
    public void MaxVirTest(){
       int exp = 30;
       int act = VirMax.virMaxresult(3,3,3);
       Assert.assertEquals(exp,act);
    }
}
