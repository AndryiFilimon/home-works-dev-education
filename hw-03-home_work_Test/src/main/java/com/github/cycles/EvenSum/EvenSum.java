package com.github.cycles.EvenSum;

public class EvenSum {
    public static void main(String[] args) {
        int[] result = OneEvenSum();
        System.out.println("Количество слагаемых от 2 до 98: " + result[0] + "\nСумма: " + result[1]);
    }

    public static int[] OneEvenSum() {
        int n = (98 - 2) / 2 + 1;
        int s = (2 + 98) / 2 * n;
        int[] result = {n, s};
        return result;
    }
}

