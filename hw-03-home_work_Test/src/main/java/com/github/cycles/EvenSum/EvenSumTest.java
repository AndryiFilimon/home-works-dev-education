package com.github.cycles.EvenSum;
import org.junit.Assert;
import org.junit.Test;

public class EvenSumTest {

    @Test
    public void SunEvenTest(){
       int[] exp = {49,2450};
       int[] act = EvenSum.OneEvenSum();
       Assert.assertEquals(exp[0],act[0]);
        Assert.assertEquals(exp[1],act[1]);
    }
}
