package com.github.cycles.PrimeCheck;

import java.util.Scanner;

public class PrimeCheck {
    public static void main(String args[]) {

        System.out.println("Введите число для проверки:");
        Scanner scan = new Scanner(System.in);
        int num = scan.nextInt();
        int result = CheckNum(num);
    }

    public static int CheckNum(int num) {
        int result = 0;
        int temp;
        boolean isPrime = true;
        for (int i = 2; i <= num / 2; i++) {
            temp = num % i;
            if (temp == 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            result = 1;
            System.out.println(num + " - простое число");
        } else {
            result = 2;
            System.out.println(num + " - составное число");
        }
        return result;
    }
}
