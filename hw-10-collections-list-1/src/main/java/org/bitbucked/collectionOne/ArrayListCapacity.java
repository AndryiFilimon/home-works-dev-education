package org.bitbucked.collectionOne;

import org.bitbucked.collectionOne.exception.ListEmptyException;

public class ArrayListCapacity implements IList {

    private int[] array = new int[0];
    private int size;

    @Override
    public void init(int[] init) {
        if (init == null) {
            this.array = new int[0];
        } else {
            this.array = new int[init.length];
            for (int i = 0; i < init.length; i++) {
                this.array[i] = init[i];
            }
        }
    }

    @Override
    public void clear() {
        this.array = new int[0];
    }

    @Override
    public int size() {
        return this.array.length;
    }

    @Override
    public int[] toArray() {
        int size = size();
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = this.array[i];
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            str.append(this.array[i]).append(", ");
        }
        return str.toString();
    }

    @Override
    public void addStart(int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[size + 1];
        for (int i = 0; i < size; i++) {
            tmp[i + 1] = this.array[i];
        }
        tmp[0] = val;
        this.array = tmp;
    }

    @Override
    public void addEnd(int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[size + 1];
        for (int i = 0; i < size; i++) {
            tmp[i] = this.array[i];
        }
        tmp[tmp.length - 1] = val;
        this.array = tmp;
    }

    @Override
    public void addByPos(int pos, int val) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[size + 1];
        for (int i = 0; i < tmp.length; i++) {
            if (i < pos) {
                tmp[i] = this.array[i];
            } else if (i > pos) {
                tmp[i] = this.array[i - 1];
            }
        }
        tmp[pos] = val;
        this.array = tmp;
    }

    @Override
    public int removeStart() {
        int size = size();
        if (size() == 0) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[size - 1];
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = this.array[i + 1];
        }
        this.array = tmp;
        return this.array.length;
    }

    @Override
    public int removeEnd() {
        int size = size();
        if (size() == 0) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[size - 1];
        for (int i = 0; i < tmp.length; i++) {
            tmp[i] = this.array[i];
        }
        this.array = tmp;
        return this.array.length;
    }

    @Override
    public int removeByPos(int pos) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int[] tmp = new int[size - 1];
        for (int i = 0; i < tmp.length; i++) {
            if (i < pos) tmp[i] = this.array[i];
            else if (pos < tmp.length) tmp[i] = this.array[i + 1];
        }
        this.array = tmp;
        return this.array.length;
    }

    @Override
    public int max() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int maxVal = this.array[0];
        for (int i = 0; i < size; i++) {
            if (maxVal < this.array[i]) {
                maxVal = this.array[i];
            }
        }
        return maxVal;
    }

    @Override
    public int min() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int minVal = this.array[0];
        for (int i = 0; i < size; i++) {
            if (minVal > this.array[i]) {
                minVal = this.array[i];
            }
        }
        return minVal;
    }

    @Override
    public int maxPos() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int pos = 0;
        int maxVal = this.array[pos];
        for (int i = 0; i < size; i++) {
            if (maxVal < this.array[i]) {
                maxVal = this.array[i];
                pos = i;
            }
        }
        return pos;
    }

    @Override
    public int minPos() {

        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int pos = 0;
        int mixVal = this.array[pos];
        for (int i = 0; i < size; i++) {
            if (mixVal < this.array[i]) {
                mixVal = this.array[i];
                pos = i;
            }
        }
        return pos;
    }

    @Override
    public int[] sort() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int p = array.length;
        for (int i = 0; i < p - 1; i++)
            for (int j = 0; j < p - i - 1; j++)
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
        return this.array;
    }

    @Override
    public int get(int pos) {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        return this.array[pos];
    }

    @Override
    public int[] halfRevers() {
        int size = size();
        if (size == 0) {
            throw new ListEmptyException();
        }
        int halfRev = (size / 2) + (size % 2);
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            if (i < halfRev) {
                array[i] = this.array[(size / 2) + i];
            } else {
                array[i] = this.array[i - halfRev];
            }
        }
        this.array = array;
        return this.array;
    }

    @Override
    public int[] revers() {
        int[] result = new int[this.size];
        for (int i = 0; i < size; i++)
            result[i] = this.array[size - 1 - i];
        return result;
    }

    @Override
    public void set(int pos, int val) {
        int size = size();
        if (pos > size || pos < 0 || size == 0) {
            throw new ListEmptyException();
        }
        this.array[pos] = val;
    }
}