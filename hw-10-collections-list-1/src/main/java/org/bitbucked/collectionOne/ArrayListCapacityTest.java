package org.bitbucked.collectionOne;

import org.bitbucked.collectionOne.exception.ListEmptyException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ArrayListCapacityTest {

    private final String name;

    private final org.bitbucked.collectionOne.IList list;

    public ArrayListCapacityTest(String name, org.bitbucked.collectionOne.IList list) {
        this.name = name;
        this.list = list;
    }

    @Before
    public void setUp() {
        this.list.clear();
    }

    @Parameterized.Parameters(name = "{index} {0}")
    public static Collection<Object[]> instances() {
        return Arrays.asList(new Object[][] {
                {"Array List Zero Capacity", new ArrayListCapacity()},
                {"Linked List Only Next", new LinkedListOnlyNext()},
        });
    }

    //=================================================
    //=================== Clean =======================
    //=================================================

    @Test
    public void clearMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void clearZero() {
        int[] array = {};
        this.list.init(array);
        this.list.clear();
        int[] exp = {};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Size ========================
    //=================================================

    @Test
    public void sizeMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 10;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 2;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    @Test
    public void sizeZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int exp = 0;
        int act = this.list.size();
        assertEquals(exp, act);
    }

    //=================================================
    //==================== AddStart ===================
    //=================================================

    @Test
    public void addToStartMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1, 1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToStartTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1, 1, 232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToStartOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToStartZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.addStart(1);
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //==================== AddEnd =====================
    //=================================================

    @Test
    public void addToEndMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.addEnd(34);
        int[] exp = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12, 34};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToEndTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.addEnd(1);
        int[] exp = new int[]{1, 232, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToEndOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.addEnd(1);
        int[] exp = new int[]{1, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addToEndZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.addEnd(1);
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== AddByPos ====================
    //=================================================

    @Test
    public void addByPoseMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.addByPos(0, 12);
        int[] exp = new int[]{12, 1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPoseTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.addByPos(2, 12);
        int[] exp = new int[]{1, 232, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPoseOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.addByPos(1, 12);
        int[] exp = new int[]{1, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void addByPoseZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.addByPos(0, 12);
        int[] exp = new int[]{12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void addByPoseIncorrect() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.addByPos(3, 12);
    }

    //=================================================
    //=================== AddByPos ====================
    //=================================================

    @Test
    public void removeStartMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.removeStart();
        int[] exp = new int []{232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeStartTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.removeStart();
        int[] exp = new int[]{232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeStartOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.removeStart();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeStartZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.removeStart();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== RemoveEnd ===================
    //=================================================

    @Test
    public void removeEndMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.removeEnd();
        int[] exp = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeEndTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.removeEnd();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeEndOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.removeEnd();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void removeEndZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.removeEnd();
    }

    //=================================================
    //================= RemoveByPos ===================
    //=================================================

    @Test
    public void removeByPoseMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.removeByPos(3);
        int[] exp = new int[]{1, 232, 43432, 543, 4343, 123, 5644, 34, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPoseTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.removeByPos(1);
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPoseOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.removeByPos(0);
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void removeByPoseZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.removeByPos(1);
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void removeByPoseIncorrect() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.removeByPos(2);
    }

    //=================================================
    //===================== Max =======================
    //=================================================

    @Test
    public void maxMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 43432;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void maxTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 232;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void maxOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    @Test
    public void maxZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int exp = 0;
        int act = this.list.max();
        assertEquals(exp, act);
    }

    //=================================================
    //===================== Min =======================
    //=================================================

    @Test
    public void minMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 1;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void minTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 1;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void minOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    @Test
    public void minZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int exp = 0;
        int act = this.list.min();
        assertEquals(exp, act);
    }

    //=================================================
    //==================== MaxPos =====================
    //=================================================

    @Test
    public void maxPosMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 2;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void maxPosTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 1;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    @Test
    public void maxPosOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 0;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }


    @Test
    public void maxPosZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int exp = 0;
        int act = this.list.maxPos();
        assertEquals(exp, act);
    }

    //=================================================
    //=================== MinPos ======================
    //=================================================

    @Test
    public void minPosMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void minPosTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 1;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void minPosOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    @Test
    public void minPosZero() {
        int[] array = new int[]{};
        this.list.init(array);
        int exp = 0;
        int act = this.list.minPos();
        assertEquals(exp, act);
    }

    //=================================================
    //==================== Sort() =====================
    //=================================================

    @Test
    public void sortMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{1, 12, 34, 123, 123, 232, 543, 4343, 5644, 43432};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortTwo() {
        int[] array = new int[]{232, 1};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{1, 232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void sortOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void sortZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.sort();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //===================== Get =======================
    //=================================================

    @Test
    public void getMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        int exp = 123;
        int act = this.list.get(3);
        assertEquals(exp, act);
    }

    @Test
    public void getTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        int exp = 232;
        int act = this.list.get(1);
        assertEquals(exp, act);
    }

    @Test
    public void getOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        int exp = 1;
        int act = this.list.get(0);
        assertEquals(exp, act);
    }


    @Test(expected = IndexOutOfBoundsException.class)
    public void getZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.get(0);
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void getIncorrect() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.get(10);
    }

    //=================================================
    //================= HalfRevers ====================
    //=================================================

    @Test
    public void halfReversMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.halfRevers();
        int[] exp = new int[]{12, 34, 5644, 123, 4343, 543, 123, 43432, 232, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.halfRevers();
        int[] exp = new int[]{232, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.halfRevers();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void halfReversZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.halfRevers();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //=================== Revers ======================
    //=================================================

    @Test
    public void reversMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.revers();
        int[] exp = new int[]{12, 34, 5644, 123, 4343, 543, 123, 43432, 232, 1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void reversTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.revers();
        int[] exp = new int[]{1, 232};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void reversOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.revers();
        int[] exp = new int[]{1};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void reversZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.revers();
        int[] exp = new int[]{};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    //=================================================
    //===================== Set =======================
    //=================================================


    @Test
    public void setMany() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.set(2, 15);
        int[] exp = new int[]{1, 232, 15, 123, 543, 4343, 123, 5644, 34, 12};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setTwo() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.set(1, 15);
        int[] exp = new int[]{1, 15};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test
    public void setOne() {
        int[] array = new int[]{1};
        this.list.init(array);
        this.list.set(0, 15);
        int[] exp = new int[]{15};
        int[] act = this.list.toArray();
        assertArrayEquals(exp, act);
    }

    @Test(expected = ListEmptyException.class)
    public void setZero() {
        int[] array = new int[]{};
        this.list.init(array);
        this.list.set(0, 15);
    }

    @Test(expected = ListEmptyException.class)
    public void setOdd() {
        int[] array = new int[]{1, 232};
        this.list.init(array);
        this.list.set(-3, 25);
    }

    @Test(expected = ListEmptyException.class)
    public void setMoreThenIndex() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.list.init(array);
        this.list.set(20, 35);
    }
}