package github.calculator;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;

public class SimpleCalcul extends JFrame {

    private JTextField tfFirstNum;

    private JTextField tfOperation;

    private JTextField tfSecondNum;

    private JTextField tfResult;

    public SimpleCalcul(ActMath ac) {
        super("Calculator");
        JPanel content = new JPanel();
        content.setLayout(null);

        JLabel lblFirstNum = new JLabel("Число 1");
        lblFirstNum.setBounds(70, 65, 195, 25);

        JLabel lblOperation = new JLabel("Операция");
        lblOperation.setBounds(70, 185, 195, 25);

        JLabel lblSecondNum = new JLabel("Число 2");
        lblSecondNum.setBounds(70, 125, 195, 25);


        JLabel lblResult = new JLabel("Результат");
        lblResult.setBounds(70, 305, 195, 25);


        this.tfFirstNum = new JTextField(20);
        tfFirstNum.setBounds(170, 60, 200, 35);

        this.tfOperation = new JTextField(20);
        tfOperation.setBounds(170, 180, 200, 35);

        this.tfSecondNum = new JTextField(20);
        tfSecondNum.setBounds(170, 120, 200, 35);

        this.tfResult = new JTextField(20);
        tfResult.setBounds(170, 300, 200, 35);

        JButton btnCalc = new JButton("Посчитать");
        btnCalc.setBounds(170, 240, 200, 35);

        ac.setTfFirstNum(this.tfFirstNum);
        ac.setTfSecondNum(this.tfSecondNum);
        ac.setTfOperation(this.tfOperation);
        ac.setTfResult(this.tfResult);

        btnCalc.addActionListener(ac.getBtnEqActionListener());

        content.add(lblFirstNum);
        content.add(lblOperation);
        content.add(lblSecondNum);
        content.add(tfFirstNum);
        content.add(tfOperation);
        content.add(tfSecondNum);
        content.add(btnCalc);

        content.add(lblResult);
        content.add(tfResult);

        setSize(550, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setContentPane(content);

        JButton button = new JButton();
        try {
            Image png = ImageIO.read(getClass().getResource("C:/Users/Андрей/Documents/home-works-dev-education/home-works-dev-education/HW-4-com.github.calculator/src/com.github.html/buttonEnter.png"));
            button.setIcon(new ImageIcon(png));
        } catch (Exception ex) {
            System.out.println(ex);
        }
        ac.add(button);
        button.setBounds(28, 243, 330, 60);
        button.addActionListener(ac.getBtnEqActionListener());


        Color color = new Color(221, 226, 206);
        Color color1 = new Color(243, 244, 238);
        ac.setBackground(color);


        Border border = new LineBorder(Color.BLACK);
        ac.setBorder(border);
        tfFirstNum.setBorder(border);
        tfSecondNum.setBorder(border);
        tfOperation.setBorder(border);
        tfResult.setBorder(border);


    }
}
