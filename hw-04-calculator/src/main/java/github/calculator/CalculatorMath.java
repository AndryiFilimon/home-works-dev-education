package github.calculator;

public class CalculatorMath {

    public static int calc(int x, int y, String op) {
        int result = 0;
        switch (op) {
            case "+":
                result = x + y;
                break;
            case "-":
                result = x - y;
                break;
            case "*":
                result = x * y;
                break;
            case "/":
                result = x / y;
                break;
        }
        return result;
    }

}
