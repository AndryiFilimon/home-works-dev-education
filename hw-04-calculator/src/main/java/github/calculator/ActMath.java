package github.calculator;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

    public class ActMath {

        private JTextField gtFirstNum;

        private JTextField tfOperation;

        private JTextField tfSecondNum;

        private JTextField tfResult;

        private final BtnEqActListener btnEqActionListener = new BtnEqActListener();

        public void setBackground(Color color) {
        }

        public void setBorder(Border border) {
            border = border;
        }

        public void add(JButton button) {
        }

        private class BtnEqActListener implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int firstNum = Integer.parseInt(gtFirstNum.getText());
                String op = tfOperation.getText();
                int secondNum = Integer.parseInt(tfSecondNum.getText());
                int result = CalculatorMath.calc(firstNum, secondNum, op);
                tfResult.setText(String.valueOf(result));
            }
        }

        public BtnEqActListener getBtnEqActionListener() {
            return btnEqActionListener;
        }

        public void setTfFirstNum(JTextField tfFirstNum) {
            this.gtFirstNum = tfFirstNum;
        }

        public void setTfOperation(JTextField tfOperation) {
            this.tfOperation = tfOperation;
        }

        public void setTfSecondNum(JTextField tfSecondNum) {
            this.tfSecondNum = tfSecondNum;
        }

        public void setTfResult(JTextField tfResult) {
            this.tfResult = tfResult;
        }
    }

