package github.calculator;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Throwable thrown) {
            thrown.printStackTrace();
        }
        SimpleCalcul abt = new SimpleCalcul(new ActMath());
        abt.setVisible(true);
    }

}

